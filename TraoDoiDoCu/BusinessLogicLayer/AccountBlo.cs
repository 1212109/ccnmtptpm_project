﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using DataAccessLayer;
using DataTransferLayer.Accounts;
using DataTransferLayer.Products;

namespace BusinessLogicLayer
{
    public static class AccountBlo
    {
        public static string Hash(string password)
        {
            var hash = Encoding.ASCII.GetBytes("san" + password);
            var sha1 = new SHA1CryptoServiceProvider();
            byte[] sha1Hash = sha1.ComputeHash(hash);
            var hashedPassword = Encoding.ASCII.GetString(sha1Hash);
            return hashedPassword;
        }

        public static List<User> GetUsers()
        {
            return AccountDao.Instance.GetUsers();
        }

        public static User GetUser(int userId)
        {
            return AccountDao.Instance.GetUser(userId);
        }

        public static User GetUser(string username)
        {
            return AccountDao.Instance.GetUser(username);
        }

        public static List<string> SearchUsernames(string content)
        {
            return AccountDao.Instance.SearchUsernames(content);
        }

        public static bool DeleteUsers(List<string> usernames)
        {
            return AccountDao.Instance.DeleteUsers(usernames);
        }

        public static bool LoginIsValid(string userName, string password)
        {
            return AccountDao.Instance.LoginIsValid(userName, Hash(password));
        }

        public static bool IsActivedAccount(string userName)
        {
            return AccountDao.Instance.IsActivedAccount(userName);
        }

        public static bool CheckExistenceAccount(string email, string userName)
        {
            return AccountDao.Instance.CheckExistenceAccount(email, userName);
        }

        public static bool RegisterAccount(User user)
        {
            var exists = AccountDao.Instance.GetUsers().
                Where(x => x.UserName == user.UserName || x.Email == user.Email).ToList();
            if (exists.Count > 0)
            {
                return false;
            }

            user.Password = Hash(user.Password);
            string result = AccountDao.Instance.AddAccount(user);
            if (result != null)
            {
                SendActivationEmail(user, result);
                return true;
            }
            return false;
        }

        // send mail active account
        private static void SendActivationEmail(User user, string activeCode)
        {
            using (MailMessage mm = new MailMessage("khoa.kiet.pttkpm@gmail.com", user.Email))
            {
                mm.Subject = "email xác nhận tài khoản trên website Trao đổi đồ cũ";
                string body = "Thân chào, " + user.UserName + ",";
                body += "<br /><br />Cảm ơn bạn đã đăng ký tài khoản trên website Trao đổi đồ cũ," 
                    + " xin hãy click vào đường link bên dưới để kích hoạt tài khoản.";
                body += "<br /><a href = 'http://" + "localhost:36984" + "/Account/Activate?Username=" 
                    + user.UserName + "&ActivationCode=" 
                    + activeCode + "'>Click vào đây để kích hoạt tài khoản.</a>";
                body += "<br /><br />Thanks";
                mm.Body = body;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential networkCred = 
                    new NetworkCredential("khoa.kiet.pttkpm@gmail.com", "khoahoangtuankiet");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = networkCred;
                smtp.Port = 587;
                smtp.Send(mm);
            }
        }

        public static bool ActiveAccount(string userName, string activationCode)
        {
            return AccountDao.Instance.ActivateAccount(userName, activationCode);
        }

        public static bool CheckMailExistence(string email)
        {
            return AccountDao.Instance.CheckMailExistence(email);
        }

        public static bool SetUpResetPassword(string email)
        {
            string resetCode = AccountDao.Instance.SetUpResetPassword(email);
            if (resetCode == null)
                return false;
            SendResetPasswordEmail(email, resetCode);
            return true;
        }

        private static void SendResetPasswordEmail(string email, string resetCode)
        {
            using (MailMessage mm = new MailMessage("khoa.kiet.pttkpm@gmail.com", email))
            {
                mm.Subject = "email reset tài khoản trên hệ thống Trao đổi đồ cũ";
                string body = "<br />Vừa có một yêu cầu lấy lại mật khẩu được gửi vào tài khoản của bạn," 
                    + " để lấy lại mật khẩu xin click vào đường link bên dưới.";
                body += "<br /><a href = http://" + "localhost:36984" + "/Account/ResetPassword?email=" 
                    + email + "&ResetCode=" + resetCode + ">Click vào đây để lấy lại mật khẩu.</a>";
                body += "<br /><br />Thanks";
                mm.Body = body;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential networkCred = 
                    new NetworkCredential("khoa.kiet.pttkpm@gmail.com", "khoahoangtuankiet");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = networkCred;
                smtp.Port = 587;
                smtp.Send(mm);
            }
        }

        public static bool CheckResetPassword(string email, string resetCode)
        {
            return AccountDao.Instance.CheckResetPassword(email, resetCode);
        }

        public static bool ChangePassword(ResetPasswordDto resetPassVm)
        {
            return AccountDao.Instance.ResetPassword(resetPassVm);
        }

        public static User GetUserInfo(int userId)
        {
            User result = AccountDao.Instance.GetUserInfo(userId);
            return result;
        }

        public static bool UpdateUserInfo(int userId, User newUpdatedUser)
        {
            return AccountDao.Instance.UpdateUserInfo(userId, newUpdatedUser);
        }

        public static bool DeleteUsers(int userId)
        {
            return AccountDao.Instance.DeleteUserAccount(userId);
        }

        public static bool CheckInvalidPassword(int userId, string password)
        {
            password = Hash(password);
            return AccountDao.Instance.CheckInvalidPassword(userId, password);
        }

        public static int GetIdFromUsername(string username)
        {
            return AccountDao.Instance.GetIdFromUsername(username);
        }

        public static List<int> GetIdProductsFollowed(string userName)
        {
            return AccountDao.Instance.GetIdProductsFollowed(userName);
        }

        public static List<Product> GetProductsFollowed(List<int> lstOfProductIDs)
        {
            return AccountDao.Instance.GetProductsFollowed(lstOfProductIDs);
        }

        public static bool DeleteAFollowedProduct(int followedProductId, int userId)
        {
            return AccountDao.Instance.DeleteAFollowedProduct(followedProductId, userId);
        }

        public static bool GetRoleUser(string username)
        {
            return AccountDao.Instance.GetRoleUser(username);
        }

        public static bool UploadAccountImage(int userId, string fileName)
        {
            return AccountDao.Instance.UploadAccountImage(userId, fileName);
        }
    }
}
