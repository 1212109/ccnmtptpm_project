﻿using System.Collections.Generic;
using System.Linq;
using DataAccessLayer;
using DataTransferLayer.Accounts;
using DataTransferLayer.Products;

namespace BusinessLogicLayer
{
    public static class ProductBlo
    {
        public static bool InilializeDatabase(string path)
        {
            return ProductDao.Instance.IniatilazeData(path);
        }
        public static List<Category> GetCategories()
        {
            return ProductDao.Instance.GetCategories();
        }

        public static List<Category> GetCategories(string name)
        {
            List<Category> cats = ProductDao.Instance.GetCategories().Where(x => x.Name == name).ToList();
            return cats;
        } 

        public static List<City> GetCities()
        {
            return ProductDao.Instance.GetCities();
        } 

        public static List<Product> GetProducts()
        {
            return ProductDao.Instance.GetProducts();
        }

        public static Product GetProduct(int productId)
        {
            return ProductDao.Instance.GetProdcut(productId);
        }

        public static Product GetLastProductBy(int userId)
        {
            if (userId > 0)
            {
                List<Product> ps = ProductDao.Instance.GetProducts().Where(x => x.UserId == userId).ToList();
                if (ps.Count > 0)
                {
                    return ps[ps.Count - 1];
                }
            }

            return null;
        }

        public static List<FollowProduct> GetFollowProducts()
        {
            return ProductDao.Instance.GetFollowProducts();
        }

        public static List<ProductImage> GetProductImages()
        {
            return ProductDao.Instance.GetProductImages();
        }

        public static bool UploadPicture(int productId, string content)
        {
            return ProductDao.Instance.UploadPicture(productId, content);
        }

        public static void AddProduct(string productName, int selectedCatId, int selectedPos,
            string desc, string productPrice, int userId, string productKey)
        {
            ProductDao.Instance.AddProduct(productName, selectedCatId, selectedPos, 
                desc, productPrice, userId, productKey);
        }

        public static bool DeleteProduct(int productId)
        {
            return ProductDao.Instance.DeleteProduct(productId);
        }

        public static bool AddPicture(int productId, int index, string fileName)
        {
            return ProductDao.Instance.AddPicture(productId, index, fileName);
        }

        public static bool EditProduct(int productId, string productName, int selectedCatId, int selectedPos,
            string desc, string productPrice, int userId, string productKey)
        {
            return ProductDao.Instance.EditProduct(productId, productName, selectedCatId, selectedPos, 
                desc, productPrice, userId, productKey);
        }

        public static bool EditPicture(int index, string fileName, Product product)
        {
            return ProductDao.Instance.EditPicture(index, fileName, product);
        }

        public static int UploadMainPicture(int pictureId)
        {
            return ProductDao.Instance.UploadMainPicture(pictureId);
        }

        public static int DeletePicture(int pictureId)
        {
            return ProductDao.Instance.DeletePicture(pictureId);
        }

        public static int DeleteMainPicture(int productId)
        {
            return ProductDao.Instance.DeleteMainPicture(productId);
        }
    }
}
