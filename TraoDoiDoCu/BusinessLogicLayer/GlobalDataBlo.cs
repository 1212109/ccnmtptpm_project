﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;

namespace BusinessLogicLayer
{
    public static class GlobalDataBlo
    {
        public static string GetNotification()
        {
            return GlobalDataDao.Instance.GetNotification();
        }

        public static void AddNotification(string content)
        {
            GlobalDataDao.Instance.AddNotification(content);
        }

        public static void DeleteAllNotification()
        {
            GlobalDataDao.Instance.DeleteAllNotification();
        }
    }
}
