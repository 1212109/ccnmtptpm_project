﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccessLayer;
using DataTransferLayer.Accounts;

namespace BusinessLogicLayer
{
    public static class UserActionBlo
    {
        public static List<Comment> GetComments()
        {
            return UserActionDao.Instance.GetComments();
        }

        public static List<Message> GetMessages()
        {
            return UserActionDao.Instance.GetMessages();
        }

        public static List<VoteLog> GetVoteLogs()
        {
            return UserActionDao.Instance.GetVoteLogs();
        } 

        public static bool AddComment(string username, int productId, string desc)
        {
            return UserActionDao.Instance.AddComment(username, productId, desc);
        }

        public static bool CheckFollow(string username, int productId)
        {
            return UserActionDao.Instance.CheckFollow(username, productId);
        }

        public static bool UnFollow(string username, int productId)
        {
            return UserActionDao.Instance.UnFollow(username, productId);
        }

        public static bool IsVoted(int sectionId, string username, int autoId)
        {
            var isIt = UserActionDao.Instance.GetVoteLogs().Where(v => v.SectionId == sectionId
                        && v.UserName.Equals(username, StringComparison.CurrentCultureIgnoreCase)
                        && v.VoteForId == autoId).FirstOrDefault();

            if (isIt != null)
            {
                return true;
            }

            return false;
        }

        public static bool SendRating(int sectionId, string username, int autoId, int thisVote)
        {
            return UserActionDao.Instance.SendRating(sectionId, username, autoId, thisVote);
        }

        public static bool ReportPost(string username, int reportForId, int productId,
            string reasonP, string reasonU, string desc)
        {
            return UserActionDao.Instance.ReportPost(username, reportForId, productId, reasonP, reasonU, desc);
        }

        public static void SendMessage(string content, int senderId, int receiverId)
        {
            UserActionDao.Instance.SendMessage(content, senderId, receiverId);
        }  

        public static void BanAccount(List<string> usernames)
        {
            UserActionDao.Instance.BanAccount(usernames);
        }

        public static void CheckBanAccount()
        {
            UserActionDao.Instance.CheckBanAccount();
        }
    }
}
