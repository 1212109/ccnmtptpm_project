﻿using System.Collections.Generic;
using DataAccessLayer;
using DataTransferLayer.Products;

namespace BusinessLogicLayer
{
    public static class HomeBlo
    {
        public static List<Product> GetProductsByCategoriesName(string name)
        {
            int id = HomeDao.Instance.GetCatIdFromCatName(name);
            return HomeDao.Instance.GetProductFromCatId(id);
        }

        public static List<Product> GetProductsByCategoriesNameAndPos(string name, string pos)
        {
            int id = HomeDao.Instance.GetCatIdFromCatName(name);
            List<Product> products = new List<Product>();

            int idPos = HomeDao.Instance.GetPosIdFromPosName(pos);
            foreach (var p in HomeDao.Instance.GetProductFromCatId(id))
            {
                if (p.Location == idPos)
                    products.Add(p);
            }

            return products;
        }
    }
}
