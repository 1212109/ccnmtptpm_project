﻿using System.Web.Mvc;

namespace TraoDoiDoCu_FrontendMVC.Models.Categories
{
    public class CategoryViewModel
    {
        public int SelectedId { get; set; }

        public SelectList Category;
    }
}