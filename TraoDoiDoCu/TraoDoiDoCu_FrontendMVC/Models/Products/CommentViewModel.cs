﻿using System.ComponentModel.DataAnnotations;

namespace TraoDoiDoCu_FrontendMVC.Models.Products
{
    [MetadataType(typeof(CommentViewModel))]
    public class Comment
    {
        internal sealed class CommentViewModel
        {
            [Display(Name = "Comment Id")]//Thuộc tính Display dùng để đặt tên lại cho cột
            public int Id { get; set; }

            [Display(Name = "Product Id")]
            public int ProductId { get; set; }

            [Display(Name = "Sender Id ")]
            public int UserId { get; set; }

            [Display(Name = "Comment Content")]
            [StringLength(50)]
            public string TextContent { get; set; }

            [Display(Name = "PostingDate")]
            [DataType(DataType.Date)]
            [DisplayFormat(DataFormatString = "{0:g}", ApplyFormatInEditMode = true)]
            public System.DateTime PostingDate { get; set; }
        }
    }
}