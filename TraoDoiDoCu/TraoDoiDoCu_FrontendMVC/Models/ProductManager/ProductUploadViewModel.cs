﻿using System.ComponentModel.DataAnnotations;

namespace TraoDoiDoCu_FrontendMVC.Models.ProductManager
{
    public class ProductUploadViewModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 0, ErrorMessage = "Tên món hàng không được bỏ trống.")]
        [Display(Name = "Tên món hàng: ")]
        public string ProductName { get; set; }

        [Display(Name = "Mô tả")]
        public string Description { get; set; }

        [Display(Name = "Chọn danh mục")]
        public int SelectedCat { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 0, ErrorMessage = "Giá sản phẩm không được bỏ trống.")]
        [Display(Name = "Giá sản phẩm: ")]
        [DataType(DataType.Password)]
        public string ProductPrice { get; set; }

        [Display(Name = "Chọn địa điểm")]
        public int SelectedPos { get; set; }

        [Display(Name = "Sản phẩm muốn trao đổi")]
        public string ProductKey { get; set; }
    }
}