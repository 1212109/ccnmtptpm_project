﻿using System.ComponentModel.DataAnnotations;

namespace TraoDoiDoCu_FrontendMVC.Models.Account
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "E-mail")]
        public string Email { get; set; }
    }
}