﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DataTransferLayer.Accounts.Api;
using PagedList;
using RestSharp;
using TraoDoiDoCu_FrontendMVC.Models.Account;
using User = DataTransferLayer.Accounts.User;

namespace TraoDoiDoCu_FrontendMVC.Controllers.Account
{
    public class AccountController : Controller
    {
        //
        // GET: /Account/login
        public ActionResult Login()
        {
            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            return View("Login");
        }

        //
        // POST: /Account/login
        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {                 
                
                if (WebApiHelper.IsValidLogin("api/accountManager/account/isValidLogin", 
                    Method.POST, model.UserName, model.Password))
                {                    
                    if(!WebApiHelper.IsActivedAccount("api/accountManager/account/isActived", 
                        Method.POST, model.UserName))
                    {
                        ModelState.AddModelError("", "Tài khoản chưa được kích hoạt");
                    }
                    else
                    {
                        User u = WebApiHelper.GetUserFromServer("api/accountManager/account/search/" 
                            + model.UserName, Method.GET);
                        int userId = u.Id;
                        Session["UserId"] = userId;

                        FormsAuthentication.SetAuthCookie(model.UserName, false);
                        bool isAdmin = u.Admin != null && u.Admin.Value;
                        if (isAdmin)
                        {
                            Session["role"] = "Admin";
                            return RedirectToAction("Index", "Dashboard");
                        }
                        else
                        {
                            Session["role"] = "NormalUser";
                            return RedirectToAction("Index", "Home");
                        }
                    }                    
                }
                else
                {
                    ModelState.AddModelError("", "Sai tên tài khoản hoặc mật khẩu");
                }
            }
            return View(model);
        }

        public ActionResult LoginWithFacebook(string accessToken)
        {

            return View();
        }

        //
        // GET: /Account/logout
        public ActionResult Logout()
        {
            if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            FormsAuthentication.SignOut();
            Session["role"] = null;
            Session["UserId"] = null;
            return View("Logout");
        }

        //
        // GET: /Account/register
        public ActionResult Register()
        {
            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            return View("Register");
        }

        //
        // Post: /Account/register
        [HttpPost]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!WebApiHelper.CheckExistenceAccount("api/accountManager/account/exists", 
                    Method.POST, model.UserName, model.Email))
                {
                    User newUser = new User();
                    newUser.UserName = model.UserName;
                    newUser.Password = model.Password;
                    newUser.Email = model.Email;
                    newUser.LastName = model.LastName;
                    newUser.FirstName = model.FirstName;
                    newUser.Phone = model.Phone;

                    if (WebApiHelper.RegisterAccount("api/accountManager/account/register", 
                        Method.POST, new UserApi(newUser)))
                    {
                        ViewBag.Message = "Tài khoản đã được ghi nhận trong hệ thống." 
                            + " Xin vui lòng kiểm tra email để kích hoạt tài khoản.";
                        return View("Result");
                    }
                    else
                    {
                        ViewBag.Alert 
                            = "Chúng tôi không thể gửi email kích hoạt đến email của bạn." 
                            + " Xin lỗi vì sự bất tiện này.";
                        return View("Result");
                    }
                }
                else
                {
                    ViewBag.Message = "Tài khoản đã tồn tại trong hệ thống.";
                    return View("Register");
                }
            }
            return View(model);
        }

        // GET: /Account/Activate
        [HttpGet]
        public ActionResult Activate(string username, string activationCode)
        {
            bool res = WebApiHelper.ActivateAccount("api/accountManager/account/active", 
                Method.POST, username, activationCode);
            ViewBag.Title = "Kết quả kích hoạt tài khoản";
            if (res)
            {                
                ViewBag.Message = "Kích hoạt tài khoản thành công." 
                    + " Bạn có thể đăng nhập tài khoản ngay bây giờ.";
                return View("Result");
            }
            else
            {
                ViewBag.Alert = "Kích hoạt tài khoản thất bại." 
                    + " Xin liên lạc với ban quản trị để biết thêm chi tiết.";
                return View("Result");
            }
        }

        // GET: /Account/ForgotPassword
        public ActionResult ForgotPassword()
        {
            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            return View("ForgotPassword");
        }

        // POST: /Account/ForgotPassword
        [HttpPost]
        public ActionResult ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (WebApiHelper.CheckExistenceEmail("api/accountManager/email/exists", Method.POST, model.Email))
                {
                    ViewBag.Title = "Kết quả lấy lại mật khẩu";
                    if (WebApiHelper.SetUpResetPassword("api/accountManager/account/setUpResetPassword", 
                        Method.POST, model.Email))
                    {
                        ViewBag.Message = "Xin vui lòng kiểm tra email để đổi lại mật khẩu.";
                        return View("Result");
                    }
                    else
                    {
                        ViewBag.Alert 
                            = "Chúng tôi không thể gửi mail lấy lại mật khẩu đến email của bạn." 
                            + " Chúng tôi xin lỗi về sự bất tiện này.";
                        return View("Result");
                    }
                }
                else
                {
                    ViewBag.Alert = "email không tồn tại.";
                    return View("ForgotPassword");
                }
            }
            return View(model);
        }

        // GET: /Account/ResetPassword
        [HttpGet]
        public ActionResult ResetPassword(string email, string resetCode)
        {
            if (WebApiHelper.CheckResetPassword("api/accountManager/account/checkResetPassword", 
                Method.POST, email, resetCode))
            {
                return View("ResetPassword");
            }
            else
            {
                ViewBag.Title = "Kết quả đổi mật khẩu";
                ViewBag.Message = "Link đổi mật khẩu đã quá hạn.";
                return View("Result");
            }
        }

        // POST: /Account/ResetPassword
        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordViewModel resetPassVm)
        {
            if (ModelState.IsValid)
            {
                ViewBag.Title = "Kết quả reset mật khẩu";

                if (WebApiHelper.ChangePassword("api/accountManager/account/changePassword", Method.GET, resetPassVm.Email, 
                    resetPassVm.Password, resetPassVm.ResetCode))
                {
                    ViewBag.Message = "Đổi khẩu thành công.";
                    return View("Result");
                }
                else
                {
                    ViewBag.Message = "Đổi mật khẩu thất bại.";
                    return View("Result");
                }
            }
            return View(resetPassVm);
        }

        // POST: /Account/DisplayUserInfo
        //[HttpPost]
        public ActionResult DisplayUserInfo()
        {
            int userId = (int)Session["UserId"];
            var user = WebApiHelper.GetUserFromServer("api/accountManager/account/" + userId, Method.GET);

            //Change code here
            return View(user);
        }

        public ActionResult DirectToEditableUserInfo()
        {
            int userId = (int)Session["UserId"];
            var user = WebApiHelper.GetUserFromServer("api/accountManager/account/" + userId, Method.GET);
            return View(user);
        }

        //[HttpPost]
        public ActionResult DeleteUser()
        {
            int userId = (int)Session["UserId"];
            var response = WebApiHelper.RequestNoFeeback("api/accountManager/account/delete/" + userId, Method.DELETE);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Session["UserId"] = null;
                return RedirectToAction("Index", "Home");
            }
            ViewData["Error"] = "Xóa người dùng không thành công";
            return RedirectToAction("DisplayUserInfo", "Account");
        }

        // GET: /Account/GetUpdatedUserInfo
        //public ActionResult UpdateUserInfo()
        //{

        //    return View();

        //}


        [HttpPost]
        public ActionResult UpdateUserInfo()
        {
            if (Request.Form.Count > 0)
            {
                UserApi updatedUserInfo = new UserApi();
                updatedUserInfo.FirstName = Request.Form["FirstName"];
                updatedUserInfo.LastName = Request.Form["LastName"];
                updatedUserInfo.Email = Request.Form["email"];
                updatedUserInfo.Phone = Request.Form["PhoneNumber"];
                updatedUserInfo.Password = Request.Form["CurrentPassword"];
                string newPassword = Request.Form["NewPassword"];
                string newPasswordConfirm = Request.Form["NewPassword_confirm"];
                if (updatedUserInfo.Password != null 
                    && updatedUserInfo.Password != newPassword 
                    && newPassword != null && newPasswordConfirm != null 
                    && newPassword == newPasswordConfirm)
                {
                    if (WebApiHelper.CheckInvalidPassword("api/accountManager/account/checkInvalidPassword", 
                        Method.POST, (int)Session["UserId"], updatedUserInfo.Password))
                    {
                        updatedUserInfo.Password = newPassword;
                    }
                }

                updatedUserInfo.Id = (int) Session["UserId"];
                WebApiHelper.UpdateUserInfo("api/accountManager/account/update", Method.POST, updatedUserInfo);
            }

            return RedirectToAction("DisplayUserInfo", "Account");
        }


        //public ActionResult DisplayFollowedProduct(string username)
        //{
        //    var lstOfFollowedProduct = bus.getProductsFollowed(bus.getIDProductsFollowed(username));
        //    return View(lstOfFollowedProduct);

        //}

        public ActionResult DisplayFollowedProduct(int? page)
        {          
            var lstOfFollowedProduct = WebApiHelper.
                GetProductsFromServer("api/accountManager/account/getProductsFollowed/" 
                + User.Identity.Name, Method.GET);
            int pageNumber = (page ?? 1);
            int pageSize = 5;

            return View(lstOfFollowedProduct.ToPagedList(pageNumber, pageSize));

        }

        public ActionResult DeleteAFollowedProduct(int followedProductId)
        {
            int userId = (int)Session["UserId"];
            WebApiHelper.DeleteAFollowedProduct("api/accountManager/account/deleteAFollowedProduct", 
                Method.POST, userId, followedProductId);
            return RedirectToAction("DisplayFollowedProduct", "Account", new { page = 1 });
        }

        public ActionResult UploadImage()
        {
            User user;
            int id = (int)Session["UserId"];
            user = WebApiHelper.GetUserFromServer("api/accountManager/account/" + id, Method.GET);
            return View(user);
        }

        [HttpPost]
        public ActionResult UploadImage(HttpPostedFileBase fileUp)
        {
            User user = new User();
            if (fileUp != null)
            {
                try
                {
                    bool flag = false;
                    /*Lopp for multiple files*/
                    /*Geting the file name*/
                    string fileName = Path.GetFileName(fileUp.FileName);
                    if (fileName != null)
                    {
                        string[] part = fileName.Split('.');
                        if (part[part.Length - 1].ToUpper() != "jpg".ToUpper() &&
                            part[part.Length - 1].ToUpper() != "png".ToUpper() &&
                            part[part.Length - 1].ToUpper() != "bmp".ToUpper())
                        {
                            flag = true;
                        }
                    }

                    if (flag)
                    {
                        ViewBag.Message = "Hình ảnh phải thuộc định dạng *.jpg, *.png hoặc *.bmp";
                    }
                    else
                    {
                        int id = (int)Session["UserId"];
                        user = WebApiHelper.GetUserFromServer("api/accountManager/account/" + id, Method.GET);

                        //fileUp.SaveAs(Server.MapPath("~/Content/images/Avatar/" + fileName));
                        //if (user.UserImage != "avatar.png")
                        //{
                        //    string fullpath = Request.MapPath("~/Content/images/Avatar/" + user.UserImage);
                        //    System.IO.File.Delete(fullpath);
                        //}

                        BinaryReader br = new BinaryReader(fileUp.InputStream);
                        byte[] bs = br.ReadBytes(fileUp.ContentLength);
                        var base64 = Convert.ToBase64String(bs);
                        string img = string.Format("data:{0};base64,{1}", "image/jpg", base64);

                        WebApiHelper.UploadAccountImage("api/accountManager/account/uploadAccountImage", 
                            Method.POST, id, img);

                        ViewBag.Message = "Avatar đã được cập nhật";
                    }
                }
                catch
                {
                    ViewBag.Message = "Có lỗi trong quá trình upload";
                }
            }
            else
            {

                ViewBag.Message = "Vui lòng chọn hình";
            }

            return View(user);
        }
	}
}