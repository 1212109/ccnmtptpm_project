﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DataTransferLayer.Products.Api;
using PagedList;
using RestSharp;
using TraoDoiDoCu_FrontendMVC.Models.ProductManager;

namespace TraoDoiDoCu_FrontendMVC.Controllers.ProductManager
{
    public class ProductManagerController : Controller
    {
        //
        // GET: /ProductManager/
        public ActionResult UpLoad()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UpLoad(ProductUploadViewModel p)
        {
            if (ModelState.IsValid)
            {
                DataTransferLayer.Accounts.User u = WebApiHelper.
                    GetUserFromServer("api/accountManager/account/search/" + User.Identity.Name, Method.GET);

                if (u != null)
                {
                    ProductApi np = new ProductApi();
                    np.Name = p.ProductName;
                    np.CategoryId = p.SelectedCat;
                    np.Location = p.SelectedPos;
                    np.Description = p.Description;
                    np.Price = decimal.Parse(p.ProductPrice);
                    np.UserId = u.Id;
                    np.ProductKey = p.ProductKey;

                    WebApiHelper.AddProductToServer("api/product/add", Method.PUT, np);

                    return RedirectToAction("UploadPicture", "ProductManager", p);
                }
                else
                {
                    FormsAuthentication.SignOut();
                    Session["role"] = null;
                    Session["UserId"] = null;
                    return RedirectToAction("Logout", "Account");
                }
                           
            }

            return View(p);
        }

        public ActionResult UploadPicture()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UploadPicture(HttpPostedFileBase[] files)
        {
            if (files != null)
            {
                try
                {
                    bool flag = false;
                    foreach (HttpPostedFileBase file in files)
                    {
                        string filename = Path.GetFileName(file.FileName);
                        if (filename != null)
                        {
                            string[] part = filename.Split('.');
                            if (part[part.Length - 1].ToUpper() != "jpg".ToUpper() 
                                && part[part.Length - 1].ToUpper() != "png".ToUpper() 
                                && part[part.Length - 1].ToUpper() != "bmp".ToUpper())
                            {
                                flag = true;
                                break;
                            }
                        }
                    }

                    if (flag)
                    {
                        ViewBag.Message = "Hình ảnh phải thuộc định dạng *.jpg, *.png hoặc *.bmp";
                    }
                    else
                    {
                        for (int i = 0; i < files.Length; )
                        {
                            int userId = (int) Session["UserId"];
                            DataTransferLayer.Products.Product p = WebApiHelper.
                                GetProductFromServer("api/product/getLast/" + userId, Method.GET);

                            BinaryReader br = new BinaryReader(files[i].InputStream);
                            byte[] bs = br.ReadBytes(files[i].ContentLength);
                            var base64 = Convert.ToBase64String(bs);
                            string img = string.Format("data:{0};base64,{1}", "image/jpg", base64);

                            WebApiHelper.AddProductPicture("api/product/productImage/add", Method.PUT, p.Id , img);

                            break;
                        }

                        ViewBag.Message = "AddProduct hình ảnh thành công";
                        ViewBag.Link = "Tiếp theo";
                    }
                }
                catch
                {
                    ViewBag.Message = "Có lỗi trong quá trình upload. Xin lỗi vì sự bất tiện này";
                }
            }
            else
            {
                ViewBag.Message = "Vui lòng chọn hình";
            }

            return View();
        }

        public ActionResult UpLoadSuccessful(ProductUploadViewModel p)
        {
            return View();
        }

        public ActionResult ListProduct(int? page)
        {
            int id = (int)Session["UserId"];
            var products = from p in WebApiHelper.GetProductsFromServer("api/product/all", Method.GET)
                           where p.UserId == id
                           select p;


            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(products.ToList().ToPagedList(pageNumber, pageSize));
        }

        public ActionResult EditProduct()
        {
            int id = int.Parse(Request.QueryString["proId"]);
            var ps = WebApiHelper.GetProductsFromServer("api/product/all", Method.GET);
            DataTransferLayer.Products.Product p = ps.FirstOrDefault(pr => pr.Id == id);
            

            ProductUploadViewModel pro = new ProductUploadViewModel();
            if (p != null)
            {
                pro.Id = p.Id;
                pro.ProductName = p.Name;
                pro.Description = p.Description;
                pro.SelectedCat = p.CategoryId;
                pro.ProductPrice = p.Price.ToString(CultureInfo.InvariantCulture);
                pro.SelectedPos = p.Location;
                pro.ProductKey = p.ProductKey;
            }

            return View(pro);
        }

        [HttpPost]
        public ActionResult EditProduct(ProductUploadViewModel p)
        {
            if (ModelState.IsValid)
            {
                ProductApi np = new ProductApi();
                np.Id = p.Id;
                np.Name = p.ProductName;
                np.CategoryId = p.SelectedCat;
                np.Location = p.SelectedPos;
                np.Description = p.Description;
                np.Price = decimal.Parse(p.ProductPrice);
                np.UserId = (int) Session["UserId"];
                np.ProductKey = p.ProductKey;

                WebApiHelper.EditProductOnServer("api/product/edit", Method.POST, np);

                return RedirectToAction("EditSuccessful", "ProductManager", p);
            }

            return View(p);
        }

        public ActionResult EditSuccessful(ProductUploadViewModel p)
        {
            return View();
        }

        public ActionResult RemoveProduct()
        {
            int id = int.Parse(Request.QueryString["proId"]);
            WebApiHelper.Request("api/product/delete/" + id, Method.DELETE);

            return RedirectToAction("ListProduct", "ProductManager");
        }

        public ActionResult ListProductKey(int? page)
        {
            var list = WebApiHelper.GetProductsFromServer("api/product/all", Method.GET);
            List<DataTransferLayer.Products.Product> result = new List<DataTransferLayer.Products.Product>();

            int id = int.Parse(Request.QueryString["proId"]);
            ViewBag.ID = id;
            string type = Request.QueryString["type"];

            var product = WebApiHelper.GetProductFromServer("api/product/" + id, Method.GET);
            if (type == "key")
            {
                string[] part = product.ProductKey.Split(' ');
                
                for (int i = 0; i < part.Length; i++)
                {
                    string key = part[i];
                    var list2 = list.Where(p => p.Name.Contains(key)).ToList();
                    foreach (var item in list2)
                    {
                        if (!result.Contains(item) && item.UserId != (int)Session["UserId"])
                        {
                            result.Add(item);
                        }
                    }
                }
            }
            else
            {
                int max;
                int min;

                if (product.Price < 1000000) 
                {
                    max = (int)product.Price + 200000;
                    min = (int)product.Price - 200000;
                }
                else
                {
                    max = (int)product.Price + 1000000;
                    min = (int)product.Price - 1000000;
                }

                if (min < 0)
                {
                    min = 0;
                }

                int idx = (int)Session["UserId"];
                var list2 = list.Where(p => (p.Price >= min && p.Price <= max && p.UserId != idx)).ToList();

                foreach (var item in list2)
                {
                    result.Add(item);
                }
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(result.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult EditPicture()
        {
            int id = int.Parse(Request.QueryString["proId"]);
            DataTransferLayer.Products.Product pro = WebApiHelper.
                GetProductsFromServer("api/product/all", Method.GET).FirstOrDefault(x => x.Id == id);

            return View(pro);
        }

        [HttpPost]
        public ActionResult EditPicture(HttpPostedFileBase[] files, int proId)
        {
            int id = proId;
            DataTransferLayer.Products.Product pro = WebApiHelper.
                GetProductsFromServer("api/product/all", Method.GET).FirstOrDefault(x => x.Id == id);
            if (files != null)
            {
                try
                {
                    bool flag = false;
                    foreach (HttpPostedFileBase file in files)
                    {
                        string filename = Path.GetFileName(file.FileName);
                        if (filename != null)
                        {
                            string[] part = filename.Split('.');
                            if (part[part.Length - 1].ToUpper() != "jpg".ToUpper() &&
                                part[part.Length - 1].ToUpper() != "png".ToUpper() &&
                                part[part.Length - 1].ToUpper() != "bmp".ToUpper())
                            {
                                flag = true;
                                break;
                            }
                        }
                    }

                    if (flag)
                    {
                        ViewBag.Message = "Hình ảnh phải thuộc định dạng *.jpg, *.png hoặc *.bmp";
                    }
                    else
                    {
                        for (int i = 0; i < files.Length;)
                        {
                            //string fileName = proId + "_" + Path.GetFileName(files[i].FileName);
                            //files[i].SaveAs(Server.MapPath("~/Content/images/ProductImage/" + fileName));

                            //ProductBlo.EditPicture(i, fileName, pro);

                            int userId = (int)Session["UserId"];
                            DataTransferLayer.Products.Product p = WebApiHelper.
                                GetProductFromServer("api/product/getLast/" + userId, Method.GET);

                            BinaryReader br = new BinaryReader(files[i].InputStream);
                            byte[] bs = br.ReadBytes(files[i].ContentLength);
                            var base64 = Convert.ToBase64String(bs);
                            string img = string.Format("data:{0};base64,{1}", "image/jpg", base64);

                            WebApiHelper.AddProductPicture("api/product/productImage/add", Method.PUT, p.Id, img);

                            break;
                        }
                        ViewBag.Message = "AddProduct hình ảnh thành công";
                    }
                }
                catch
                {
                    ViewBag.Message = "Có lỗi trong quá trình upload. Xin lỗi vì sự bất tiện này";
                }
            }
            else
            {
                ViewBag.Message = "Vui lòng chọn hình";
            }

            return View(pro);
        }

        public ActionResult ReplaceMainPicture()
        {
            int id = int.Parse(Request.QueryString["id"]);

            int productId = -1;
            var ints = WebApiHelper.GetIntsFromServer("api/product/productImage/replaceMain/" + id, Method.PUT);
            foreach (var i in ints)
            {
                productId = i;
                break;
            }

            return RedirectToAction("EditPicture", "ProductManager", new { ProID = productId });
        }

        public ActionResult RemoveThePicture()
        {
            int id = int.Parse(Request.QueryString["id"]);

            int productId = -1;
            var ints = WebApiHelper.GetIntsFromServer("api/product/productImage/replaceMain/" + id, Method.PUT);
            foreach (var i in ints)
            {
                productId = i;
                break;
            }

            return RedirectToAction("EditPicture", "ProductManager", new { ProID = productId });
        }

        public ActionResult RemoveTheMainPicture()
        {
            int id = int.Parse(Request.QueryString["id"]);

            int productId = -1;
            var ints = WebApiHelper.GetIntsFromServer("api/product/productImage/deleteMain/" + id, Method.PUT);
            foreach (var i in ints)
            {
                productId = i;
                break;
            }

            return RedirectToAction("EditPicture", "ProductManager", new { ProID = productId });
        }
	}
}