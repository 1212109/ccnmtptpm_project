﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using DataTransferLayer.Accounts;
using PagedList;
using RestSharp;
using HttpCookie = System.Web.HttpCookie;

namespace TraoDoiDoCu_FrontendMVC.Controllers.Product
{
    public class ProductController : Controller
    {
        // GET: Product/Details/5
        public ActionResult Details(int id, int? page)
        {
            ViewBag.page = page;
            ViewBag.product = id;
            ViewBag.follow = false;
            if (IsFollowing(User.Identity.Name, id))
                ViewBag.follow = true;

            var ps = WebApiHelper.GetProductsFromServer("api/product/all", Method.GET);
            DataTransferLayer.Products.Product product = ps.FirstOrDefault(p => p.Id == id);

            if (product != null)
            {
                var u = WebApiHelper.GetUserFromServer("api/accountManager/account/" + product.UserId, Method.GET);
                ViewBag.PosterName = u.LastName + " " + u.FirstName;
                ViewBag.CatName = product.Category.Name;

                return View(product);
            }

            return HttpNotFound();
        }

        public bool IsFollowing(string username, int productId)
        {
            int id;
            var users = WebApiHelper.GetUsersFromServer("api/accountManager/account/all", Method.GET);
            User target = users.FirstOrDefault(x => x.UserName == username);

            if (target != null)
            {
                var fps = WebApiHelper.GetFollowProductsFromServer("api/product/followProduct/all", Method.GET);

                id = target.Id;
                var fp = fps.Where(x => x.ProductId == productId && x.UserId == id && x.Active).SingleOrDefault();
                if (fp != null)
                    return true;
            }

            return false;
        }

        public PartialViewResult PosterInfoPartial(int posterId, int productId)
        {
            ViewBag.productID = productId;

            User poster = WebApiHelper.GetUsersFromServer("api/accountManager/account/all", Method.GET).
                Where(p => p.Id == posterId).FirstOrDefault();

            return PartialView(poster);
        }

        public PartialViewResult ProductImagePartial(int productId)
        {
            return PartialView(WebApiHelper.GetProductImagesFromServer("api/product/productImage/all", Method.GET).
                Where(p => p.ProductId == productId).OrderBy(x => x.Id).ToList());
        }

        public PartialViewResult ProductCommentPartial(int productId, int? page)
        {
            ViewBag.productID = productId;
            ViewBag.page = page;

            int pageNumber = (page ?? 1);
            int pageSize = 10;

            return PartialView(WebApiHelper.GetCommentsFromServer("api/userAction/comment/all", Method.GET).
                Where(p => p.ProductId == productId).
                OrderBy(x => x.Id).ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult SubmitComment(string desc, int productId, int? page)
        {
            ViewBag.productID = productId;
            ViewBag.page = page;
            
            if (desc == "")
                return Json("Bình luận không được để trống");

            if (ModelState.IsValid)
            {
                var respone = WebApiHelper.RequestNoFeeback("api/userAction/comment/add/" 
                    + User.Identity.Name + WebApiHelper.Seperator 
                    + productId + WebApiHelper.Seperator + desc, Method.POST);
                if(respone.StatusCode == HttpStatusCode.OK)
                {
                    return Json(true);
                }          
            }

            return Json(false);
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult Checkfollow(int productId, int? page)
        {
            ViewBag.productID = productId;
            ViewBag.page = page;
            
            if (ModelState.IsValid)
            {
                var respone = WebApiHelper.RequestNoFeeback("api/userAction/follow/check/"
                    + User.Identity.Name + WebApiHelper.Seperator + productId, Method.POST);
                if (respone.StatusCode == HttpStatusCode.OK)
                {
                    return Json(true);
                }             
            }

            return Json(false);
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult Unfollow(int productId, int? page)
        {
            ViewBag.productID = productId;
            ViewBag.page = page;
            
            if (ModelState.IsValid)
            {
                var respone = WebApiHelper.RequestNoFeeback("api/userAction/follow/unfollow/"
                    + User.Identity.Name + WebApiHelper.Seperator + productId, Method.POST);
                if (respone.StatusCode == HttpStatusCode.OK)
                {
                    return Json(true);
                }
            }

            return Json(false);
        }

        public JsonResult SendRating(string r, string s, string id, string url)
        {
            int autoId;
            int thisVote;
            int sectionId;
            int.TryParse(s, out sectionId);
            int.TryParse(r, out thisVote);
            int.TryParse(id, out autoId);            

            if (!User.Identity.IsAuthenticated)
            {
                return Json("Not authenticated!");
            }

            if (autoId.Equals(0))
            {
                return Json("Sorry, record to vote doesn't exists");
            }

            switch (s)
            {
                case "5": // school voting
                    // check if he has already voted
                    var respone = WebApiHelper.RequestNoFeeback("api/userAction/vote/isVoted/"
                        + sectionId + WebApiHelper.Seperator 
                        + User.Identity.Name + WebApiHelper.Seperator + autoId, Method.POST);
                    if (respone.StatusCode == HttpStatusCode.OK)
                    {
                        // keep the school voting flag to stop voting by this member
                        HttpCookie cookie = new HttpCookie(url, "true");
                        Response.Cookies.Add(cookie);

                        return Json("<br />You have already rated this post, thanks !");
                    }

                    respone = WebApiHelper.RequestNoFeeback("api/userAction/rating/send/" 
                        + int.Parse(s) + WebApiHelper.Seperator 
                        + User.Identity.Name + WebApiHelper.Seperator 
                        + autoId + WebApiHelper.Seperator + thisVote, Method.POST);
                    if (respone.StatusCode == HttpStatusCode.OK)
                    {
                        // keep the school voting flag to stop voting by this member
                        HttpCookie cookie = new HttpCookie(url, "true");
                        Response.Cookies.Add(cookie);
                    }

                    break;
            }

            return Json("<br />You rated " + r + " star(s), thanks !");
        }

        public JsonResult PostReport(int reportForId, int productId, string reasonP, string reasonU, string desc)
        {
            var u = (from d in WebApiHelper.GetUsersFromServer("api/accountManager/account/all", Method.GET)
                     where d.UserName == User.Identity.Name
                     select new { d.Id }).SingleOrDefault();

            if (reasonP == "" && reasonU == "")
                return Json("Vui lòng chọn nguyên nhân");

            if (u == null)
                return Json("Không thể gửi report vui lòng thử lại");

            var respone = WebApiHelper.RequestNoFeeback("api/userAction/report/reportFor/"
                        + User.Identity.Name + WebApiHelper.Seperator
                        + reportForId + WebApiHelper.Seperator
                        + productId + WebApiHelper.Seperator 
                        + reasonP + WebApiHelper.Seperator 
                        + reasonU + WebApiHelper.Seperator + desc, Method.POST);
            if (respone.StatusCode == HttpStatusCode.OK)
            {
                return Json("Bạn đã report bài viết này");
            }

            return Json(true);
        }
    }
}
