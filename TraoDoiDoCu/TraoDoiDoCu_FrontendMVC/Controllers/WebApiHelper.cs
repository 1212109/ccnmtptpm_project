﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using DataTransferLayer.Accounts;
using DataTransferLayer.Accounts.Api;
using DataTransferLayer.Products;
using DataTransferLayer.Products.Api;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace TraoDoiDoCu_FrontendMVC.Controllers
{
    public static class WebApiHelper
    {
        public static string Seperator = "055512022lol";
        public static string ClientBase = "http://localhost:14980/";

        //static byte[] GetBytes(string str)
        //{
        //    byte[] bytes = new byte[str.Length * sizeof(char)];
        //    Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
        //    return bytes;
        //}

        public static string Request(string requestResource, Method method)
        {
            var client = new RestClient();
            // This, of course, needs to be altered to match the
            // IP Address/Port of the server to which you are connecting
            client.BaseUrl = new Uri(ClientBase);
            var request = new RestRequest();
            // The server's Rest method will probably return something
            // other than "duckbilledPlatypi" in your case
            request.Resource = requestResource;
            request.Method = method;
            var response = client.Execute(request) as RestResponse;

            if (response != null 
                && ((response.StatusCode == HttpStatusCode.OK) 
                    && (response.ResponseStatus == ResponseStatus.Completed))) 
                // It's probably not necessary to test both
            {
                return response.Content;   
            }
            else 
                if (response != null)
                {
                    throw new Exception(string.
                        Format("Status code is {0} ({1}); response status is {2}", 
                            response.StatusCode, response.StatusDescription, response.ResponseStatus));
                }

            return null;
        }

        public static RestResponse RequestNoFeeback(string requestResource, Method method)
        {
            var client = new RestClient();
            // This, of course, needs to be altered to match the
            // IP Address/Port of the server to which you are connecting
            client.BaseUrl = new Uri(ClientBase);
            var request = new RestRequest();
            // The server's Rest method will probably return something
            // other than "duckbilledPlatypi" in your case
            request.Resource = requestResource;
            request.Method = method;
            var response = client.Execute(request) as RestResponse;

            return response;
        }

        public static List<string> GetStringsFromServer(string request, Method method)
        {
            var strings = new List<string>();

            var results = Request(request, method);
            try
            {
                JArray ja = JsonConvert.DeserializeObject<JArray>(results);
                foreach (var r in ja)
                {
                    string s = (string) r;
                    strings.Add(s);
                }
            }
            catch (Exception ex1)
            {
                Console.WriteLine(ex1.ToString());

                try
                {
                    JObject jo = JsonConvert.DeserializeObject<JObject>(results);
                    string s = (string) jo;
                    strings.Add(s);
                }
                catch (Exception ex2)
                {
                    Console.WriteLine(ex2.ToString());
                }
            }
            
            return strings;
        }

        public static List<int> GetIntsFromServer(string request, Method method)
        {
            var ints = new List<int>();

            var results = Request(request, method);
            try
            {
                JArray ja = JsonConvert.DeserializeObject<JArray>(results);
                foreach (var r in ja)
                {
                    int s = (int)r;
                    ints.Add(s);
                }
            }
            catch (Exception ex1)
            {
                Console.WriteLine(ex1.ToString());

                try
                {
                    JObject r = JsonConvert.DeserializeObject<JObject>(results);
                    int s = (int) r;
                    ints.Add(s);
                }
                catch (Exception ex2)
                {
                    Console.WriteLine(ex2.ToString());
                }
            }
            
            return ints;
        }

        public static List<Category> GetCategoriesFromServer(string request, Method method)
        {
            var cats = new List<Category>();

            var results = Request(request, method);
            try
            {
                JArray ja = JsonConvert.DeserializeObject<JArray>(results);
                foreach (var r in ja)
                {
                    var c = (CategoryApi)JsonConvert.DeserializeObject(r.ToString(), typeof(CategoryApi));
                    var p = new Category(c);
                    cats.Add(p);
                }
            }
            catch (Exception ex1)
            {
                Console.WriteLine(ex1.ToString());

                try
                {
                    JObject r = JsonConvert.DeserializeObject<JObject>(results);
                    var c = (CategoryApi)JsonConvert.DeserializeObject(r.ToString(), typeof(CategoryApi));
                    var p = new Category(c);
                    cats.Add(p);
                }
                catch (Exception ex2)
                {
                    Console.WriteLine(ex2.ToString());
                }
            }           

            return cats;
        }

        public static List<City> GetCitiesFromServer(string request, Method method)
        {
            var cities = new List<City>();

            var results = Request(request, method);
            try
            {
                JArray ja = JsonConvert.DeserializeObject<JArray>(results);
                foreach (var r in ja)
                {
                    var c = (CityApi)JsonConvert.DeserializeObject(r.ToString(), typeof(CityApi));
                    var p = new City(c);
                    cities.Add(p);
                }
            }
            catch (Exception ex1)
            {
                Console.WriteLine(ex1.ToString());

                try
                {
                    JObject r = JsonConvert.DeserializeObject<JObject>(results);
                    var c = (CityApi)JsonConvert.DeserializeObject(r.ToString(), typeof(CityApi));
                    var p = new City(c);
                    cities.Add(p);
                }
                catch (Exception ex2)
                {
                    Console.WriteLine(ex2.ToString());
                }
            }        

            return cities;
        }

        public static List<DataTransferLayer.Products.Product> GetProductsFromServer(string request, Method method)
        {
            var products = new List<DataTransferLayer.Products.Product>();

            List<City> cities = GetCitiesFromServer("api/product/cities", Method.GET);
            List<Category> categories = GetCategoriesFromServer("api/product/categories", Method.GET);

            var results = Request(request, method);
            try
            {
                JArray ja = JsonConvert.DeserializeObject<JArray>(results);
                foreach (var r in ja)
                {
                    var c = (ProductApi)JsonConvert.DeserializeObject(r.ToString(), typeof(ProductApi));
                    var p = new DataTransferLayer.Products.Product(c);

                    var cs = cities.Where(x => x.Id == p.Location).ToList();
                    if (cs.Count > 0)
                    {
                        p.City = cs[0];
                    }

                    var ccs = categories.Where(x => x.Id == p.CategoryId).ToList();
                    if (ccs.Count > 0)
                    {
                        p.Category = ccs[0];
                    }

                    products.Add(p);
                }
            }
            catch (Exception ex1)
            {
                Console.WriteLine(ex1.ToString());

                try
                {
                    JObject r = JsonConvert.DeserializeObject<JObject>(results);
                    var c = (ProductApi)JsonConvert.DeserializeObject(r.ToString(), typeof(ProductApi));
                    var p = new DataTransferLayer.Products.Product(c);

                    var cs = cities.Where(x => x.Id == p.Location).ToList();
                    if (cs.Count > 0)
                    {
                        p.City = cs[0];
                    }

                    var ccs = categories.Where(x => x.Id == p.CategoryId).ToList();
                    if (ccs.Count > 0)
                    {
                        p.Category = ccs[0];
                    }

                    products.Add(p);
                }
                catch (Exception ex2)
                {
                    Console.WriteLine(ex2.ToString());
                }
            }          

            return products;
        }

        public static DataTransferLayer.Products.Product GetProductFromServer(string request, Method method)
        {
            var product = new DataTransferLayer.Products.Product();

            List<City> cities = GetCitiesFromServer("api/product/cities", Method.GET);
            List<Category> categories = GetCategoriesFromServer("api/product/categories", Method.GET);

            var results = Request(request, method);
            try
            {
                JArray ja = JsonConvert.DeserializeObject<JArray>(results);
                foreach (var r in ja)
                {
                    var c = (ProductApi)JsonConvert.DeserializeObject(r.ToString(), typeof(ProductApi));
                    var p = new DataTransferLayer.Products.Product(c);

                    var cs = cities.Where(x => x.Id == p.Location).ToList();
                    if (cs.Count > 0)
                    {
                        p.City = cs[0];
                    }

                    var ccs = categories.Where(x => x.Id == p.CategoryId).ToList();
                    if (ccs.Count > 0)
                    {
                        p.Category = ccs[0];
                    }

                    return p;
                }
            }
            catch (Exception ex1)
            {
                Console.WriteLine(ex1.ToString());

                try
                {
                    JObject r = JsonConvert.DeserializeObject<JObject>(results);

                    var c = (ProductApi)JsonConvert.DeserializeObject(r.ToString(), typeof(ProductApi));
                    var p = new DataTransferLayer.Products.Product(c);

                    var cs = cities.Where(x => x.Id == p.Location).ToList();
                    if (cs.Count > 0)
                    {
                        p.City = cs[0];
                    }

                    var ccs = categories.Where(x => x.Id == p.CategoryId).ToList();
                    if (ccs.Count > 0)
                    {
                        p.Category = ccs[0];
                    }

                    return p;
                }
                catch (Exception ex2)
                {
                    Console.WriteLine(ex2.ToString());
                }
            }         

            return product;
        }

        public static bool AddProductToServer(string requestResource, Method method, ProductApi product)
        {
            var client = new RestClient();
            // This, of course, needs to be altered to match the
            // IP Address/Port of the server to which you are connecting
            client.BaseUrl = new Uri(ClientBase);
            var request = new RestRequest();
            // The server's Rest method will probably return something
            // other than "duckbilledPlatypi" in your case
            request.Resource = requestResource + "/" 
                + product.Name + Seperator 
                + product.CategoryId + Seperator
                + product.Location + Seperator
                + product.Description + Seperator
                + product.Price + Seperator
                + product.UserId + Seperator
                + product.ProductKey;
            request.Method = method;

            var response = client.Execute(request) as RestResponse;

            if (response != null
                && ((response.StatusCode == HttpStatusCode.OK)
                    && (response.ResponseStatus == ResponseStatus.Completed)))
            // It's probably not necessary to test both
            {
                return true;
            }
            else
                if (response != null)
                {
                    return false;
                }

            return false;
        }

        public static bool EditProductOnServer(string requestResource, Method method, ProductApi product)
        {
            var client = new RestClient();
            // This, of course, needs to be altered to match the
            // IP Address/Port of the server to which you are connecting
            client.BaseUrl = new Uri(ClientBase);
            var request = new RestRequest();
            // The server's Rest method will probably return something
            // other than "duckbilledPlatypi" in your case
            request.Resource = requestResource + "/"
                + product.Id + Seperator
                + product.Name + Seperator
                + product.CategoryId + Seperator
                + product.Location + Seperator
                + product.Description + Seperator
                + product.Price + Seperator
                + product.UserId + Seperator
                + product.ProductKey;
            request.Method = method;

            var response = client.Execute(request) as RestResponse;

            if (response != null
                && ((response.StatusCode == HttpStatusCode.OK)
                    && (response.ResponseStatus == ResponseStatus.Completed)))
            // It's probably not necessary to test both
            {
                return true;
            }
            else
                if (response != null)
                {
                    return false;
                }

            return false;
        }

        public static bool AddProductPicture(string requestResource, Method method, int productId, string content)
        {
            var client = new RestClient();
            // This, of course, needs to be altered to match the
            // IP Address/Port of the server to which you are connecting
            client.BaseUrl = new Uri(ClientBase);
            var request = new RestRequest();
            // The server's Rest method will probably return something
            // other than "duckbilledPlatypi" in your case
            request.Resource = requestResource + "/"
                + productId + "?content=" + content;
            request.Method = method;

            var response = client.Execute(request) as RestResponse;

            if (response != null
                && ((response.StatusCode == HttpStatusCode.OK)
                    && (response.ResponseStatus == ResponseStatus.Completed)))
            // It's probably not necessary to test both
            {
                return true;
            }
            else
                if (response != null)
                {
                    return false;
                }

            return false;
        }

        public static List<FollowProduct> GetFollowProductsFromServer(string request, Method method)
        {
            var products = new List<FollowProduct>();

            var results = Request(request, method);
            try
            {
                JArray ja = JsonConvert.DeserializeObject<JArray>(results);
                foreach (var r in ja)
                {
                    var c = (FollowProductApi)JsonConvert.DeserializeObject(r.ToString(), typeof(FollowProductApi));
                    var p = new FollowProduct(c);
                    products.Add(p);
                }
            }
            catch (Exception ex1)
            {
                Console.WriteLine(ex1.ToString());

                try
                {
                    JObject r = JsonConvert.DeserializeObject<JObject>(results);
                    var c = (FollowProductApi)JsonConvert.DeserializeObject(r.ToString(), typeof(FollowProductApi));
                    var p = new FollowProduct(c);
                    products.Add(p);   
                }
                catch (Exception ex2)
                {
                    Console.WriteLine(ex2.ToString());
                }
            }

            return products;
        }

        public static List<ProductImage> GetProductImagesFromServer(string request, Method method)
        {
            var products = new List<ProductImage>();

            var results = Request(request, method);
            try
            {
                JArray ja = JsonConvert.DeserializeObject<JArray>(results);
                foreach (var r in ja)
                {
                    var c = (ProductImageApi)JsonConvert.DeserializeObject(r.ToString(), typeof(ProductImageApi));
                    var p = new ProductImage(c);
                    products.Add(p);
                }
            }
            catch (Exception ex1)
            {
                Console.WriteLine(ex1.ToString());

                try
                {
                    JObject r = JsonConvert.DeserializeObject<JObject>(results);
                    var c = (ProductImageApi)JsonConvert.DeserializeObject(r.ToString(), typeof(ProductImageApi));
                    var p = new ProductImage(c);
                    products.Add(p);
                }
                catch (Exception ex2)
                {
                    Console.WriteLine(ex2.ToString());
                }
            }          

            return products;
        }

        public static List<VoteLog> GetVoteLogsFromServer(string request, Method method)
        {
            var votes = new List<VoteLog>();

            var results = Request(request, method);
            try
            {
                JArray ja = JsonConvert.DeserializeObject<JArray>(results);
                foreach (var r in ja)
                {
                    var c = (VoteLogApi)JsonConvert.DeserializeObject(r.ToString(), typeof(VoteLogApi));
                    var p = new VoteLog(c);
                    votes.Add(p);
                }
            }
            catch (Exception ex1)
            {
                Console.WriteLine(ex1.ToString());

                try
                {
                    JObject r = JsonConvert.DeserializeObject<JObject>(results);
                    var c = (VoteLogApi)JsonConvert.DeserializeObject(r.ToString(), typeof(VoteLogApi));
                    var p = new VoteLog(c);
                    votes.Add(p);
                }
                catch (Exception ex2)
                {
                    Console.WriteLine(ex2.ToString());
                }
            }            

            return votes;
        }

        public static List<User> GetUsersFromServer(string request, Method method)
        {
            var users = new List<User>();

            var results = Request(request, method);
            try
            {
                JArray ja = JsonConvert.DeserializeObject<JArray>(results);
                foreach (var r in ja)
                {
                    var c = (UserApi)JsonConvert.DeserializeObject(r.ToString(), typeof(UserApi));
                    var u = new User(c);
                    users.Add(u);
                }
            }
            catch (Exception ex1)
            {
                Console.WriteLine(ex1.ToString());

                try
                {
                    JObject r = JsonConvert.DeserializeObject<JObject>(results);
                    var c = (UserApi)JsonConvert.DeserializeObject(r.ToString(), typeof(UserApi));
                    var u = new User(c);
                    users.Add(u);
                }
                catch (Exception ex2)
                {
                    Console.WriteLine(ex2.ToString());
                }
            }           

            return users;
        }

        public static User GetUserFromServer(string request, Method method)
        {
            User user = new User();

            var results = Request(request, method);
            try
            {
                JArray ja = JsonConvert.DeserializeObject<JArray>(results);
                foreach (var r in ja)
                {
                    var c = (UserApi)JsonConvert.DeserializeObject(r.ToString(), typeof(UserApi));
                    user = new User(c);
                    break;
                }

            }
            catch (Exception ex1)
            {
                Console.WriteLine(ex1.ToString());

                try
                {
                    JObject r = JsonConvert.DeserializeObject<JObject>(results);
                    var c = (UserApi)JsonConvert.DeserializeObject(r.ToString(), typeof(UserApi));
                    user = new User(c);
                }
                catch (Exception ex2)
                {
                    Console.WriteLine(ex2.ToString());
                }
            }
                      
            return user;
        }

        public static List<Comment> GetCommentsFromServer(string request, Method method)
        {
            var comments = new List<Comment>();

            var results = Request(request, method);
            try
            {
                JArray ja = JsonConvert.DeserializeObject<JArray>(results);
                foreach (var r in ja)
                {
                    var c = (CommentApi)JsonConvert.DeserializeObject(r.ToString(), typeof(CommentApi));
                    var u = new Comment(c);
                    comments.Add(u);
                }

            }
            catch (Exception ex1)
            {
                Console.WriteLine(ex1.ToString());

                try
                {
                    JObject r = JsonConvert.DeserializeObject<JObject>(results);
                    var c = (CommentApi)JsonConvert.DeserializeObject(r.ToString(), typeof(CommentApi));
                    var u = new Comment(c);
                    comments.Add(u);
                }
                catch (Exception ex2)
                {
                    Console.WriteLine(ex2.ToString());
                }
            }          

            return comments;
        }

        public static List<Message> GetMessagesFromServer(string request, Method method)
        {
            var messages = new List<Message>();

            var results = Request(request, method);
            try
            {
                JArray ja = JsonConvert.DeserializeObject<JArray>(results);
                foreach (var r in ja)
                {
                    var c = (MessageApi)JsonConvert.DeserializeObject(r.ToString(), typeof(MessageApi));
                    var u = new Message(c);
                    messages.Add(u);
                }

            }
            catch (Exception ex1)
            {
                Console.WriteLine(ex1.ToString());

                try
                {
                    JObject r = JsonConvert.DeserializeObject<JObject>(results);
                    var c = (MessageApi)JsonConvert.DeserializeObject(r.ToString(), typeof(MessageApi));
                    var u = new Message(c);
                    messages.Add(u);
                }
                catch (Exception ex2)
                {
                    Console.WriteLine(ex2.ToString());
                }
            }
            
            return messages;
        }

        public static bool IsValidLogin(string requestResource, Method method, 
            string userName, string password)
        {
            var client = new RestClient();
            // This, of course, needs to be altered to match the
            // IP Address/Port of the server to which you are connecting
            client.BaseUrl = new Uri(ClientBase);
            var request = new RestRequest();
            // The server's Rest method will probably return something
            // other than "duckbilledPlatypi" in your case
            request.Resource = requestResource + "/" + userName + Seperator + password;
            request.Method = method;

            var response = client.Execute(request) as RestResponse;

            if (response != null
                && ((response.StatusCode == HttpStatusCode.OK)
                    && (response.ResponseStatus == ResponseStatus.Completed)))
            // It's probably not necessary to test both
            {
                return true;
            }
            else
                if (response != null)
            {
                return false;
            }

            return false;
        }

        public static bool IsActivedAccount(string requestResource, Method method, string userName)
        {
            var client = new RestClient();
            // This, of course, needs to be altered to match the
            // IP Address/Port of the server to which you are connecting
            client.BaseUrl = new Uri(ClientBase);
            var request = new RestRequest();
            // The server's Rest method will probably return something
            // other than "duckbilledPlatypi" in your case
            request.Resource = requestResource + "/" + userName;
            request.Method = method;

            var response = client.Execute(request) as RestResponse;

            if (response != null
                && ((response.StatusCode == HttpStatusCode.OK)
                    && (response.ResponseStatus == ResponseStatus.Completed)))
            // It's probably not necessary to test both
            {
                return true;
            }
            else
                if (response != null)
                {
                    return false;
                }

            return false;
        }

        public static bool CheckExistenceAccount(string requestResource, Method method, string userName, string email)
        {
            var client = new RestClient();
            // This, of course, needs to be altered to match the
            // IP Address/Port of the server to which you are connecting
            client.BaseUrl = new Uri(ClientBase);
            var request = new RestRequest();
            // The server's Rest method will probably return something
            // other than "duckbilledPlatypi" in your case
            request.Resource = requestResource + "/" + userName + "?email=" + email;
            request.Method = method;

            var response = client.Execute(request) as RestResponse;

            if (response != null
                && ((response.StatusCode == HttpStatusCode.OK)
                    && (response.ResponseStatus == ResponseStatus.Completed)))
            // It's probably not necessary to test both
            {
                return true;
            }
            else
                if (response != null)
                {
                    return false;
                }

            return false;
        }

        public static bool CheckExistenceEmail(string requestResource, Method method, string email)
        {
            var client = new RestClient();
            // This, of course, needs to be altered to match the
            // IP Address/Port of the server to which you are connecting
            client.BaseUrl = new Uri(ClientBase);
            var request = new RestRequest();
            // The server's Rest method will probably return something
            // other than "duckbilledPlatypi" in your case
            request.Resource = requestResource + "/sdfsdf?email=" + email;
            request.Method = method;

            var response = client.Execute(request) as RestResponse;

            if (response != null
                && ((response.StatusCode == HttpStatusCode.OK)
                    && (response.ResponseStatus == ResponseStatus.Completed)))
            // It's probably not necessary to test both
            {
                return true;
            }
            else
                if (response != null)
                {
                    return false;
                }

            return false;
        }

        public static bool RegisterAccount(string requestResource, Method method, UserApi newUser)
        {
            var client = new RestClient();
            // This, of course, needs to be altered to match the
            // IP Address/Port of the server to which you are connecting
            client.BaseUrl = new Uri(ClientBase);
            var request = new RestRequest();
            // The server's Rest method will probably return something
            // other than "duckbilledPlatypi" in your case
            request.Resource = requestResource + "/"
                + newUser.UserName + Seperator
                + newUser.Password + Seperator
                + " " + Seperator
                + newUser.LastName + Seperator
                + newUser.FirstName + Seperator
                + newUser.Phone
                + "?email=" + newUser.Email;
            request.Method = method;

            var response = client.Execute(request) as RestResponse;

            if (response != null
                && ((response.StatusCode == HttpStatusCode.OK)
                    && (response.ResponseStatus == ResponseStatus.Completed)))
            // It's probably not necessary to test both
            {
                return true;
            }
            else
                if (response != null)
                {
                    return false;
                }

            return false;
        }

        public static bool ActivateAccount(string requestResource, Method method, 
            string userName, string activationCode)
        {
            var client = new RestClient();
            // This, of course, needs to be altered to match the
            // IP Address/Port of the server to which you are connecting
            client.BaseUrl = new Uri(ClientBase);
            var request = new RestRequest();
            // The server's Rest method will probably return something
            // other than "duckbilledPlatypi" in your case
            request.Resource = requestResource + "/"
                + userName + "?activationCode=" + activationCode;
            request.Method = method;

            var response = client.Execute(request) as RestResponse;

            if (response != null
                && ((response.StatusCode == HttpStatusCode.OK)
                    && (response.ResponseStatus == ResponseStatus.Completed)))
            // It's probably not necessary to test both
            {
                return true;
            }
            else
                if (response != null)
                {
                    return false;
                }

            return false;
        }

        public static bool SetUpResetPassword(string requestResource, Method method, string email)
        {
            var client = new RestClient();
            // This, of course, needs to be altered to match the
            // IP Address/Port of the server to which you are connecting
            client.BaseUrl = new Uri(ClientBase);
            var request = new RestRequest();
            // The server's Rest method will probably return something
            // other than "duckbilledPlatypi" in your case
            request.Resource = requestResource + "/dfgdf?email=" + email;
            request.Method = method;

            var response = client.Execute(request) as RestResponse;

            if (response != null
                && ((response.StatusCode == HttpStatusCode.OK)
                    && (response.ResponseStatus == ResponseStatus.Completed)))
            // It's probably not necessary to test both
            {
                return true;
            }
            else
                if (response != null)
                {
                    return false;
                }

            return false;
        }

        public static bool CheckResetPassword(string requestResource, Method method, 
            string email, string resetCode)
        {
            var client = new RestClient();
            // This, of course, needs to be altered to match the
            // IP Address/Port of the server to which you are connecting
            client.BaseUrl = new Uri(ClientBase);
            var request = new RestRequest();
            // The server's Rest method will probably return something
            // other than "duckbilledPlatypi" in your case
            request.Resource = requestResource + "/" + resetCode + "?email=" + email;
            request.Method = method;

            var response = client.Execute(request) as RestResponse;

            if (response != null
                && ((response.StatusCode == HttpStatusCode.OK)
                    && (response.ResponseStatus == ResponseStatus.Completed)))
            // It's probably not necessary to test both
            {
                return true;
            }
            else
                if (response != null)
                {
                    return false;
                }

            return false;
        }

        public static bool ChangePassword(string requestResource, Method method,
            string email, string password, string resetCode)
        {
            var client = new RestClient();
            // This, of course, needs to be altered to match the
            // IP Address/Port of the server to which you are connecting
            client.BaseUrl = new Uri(ClientBase);
            var request = new RestRequest();
            // The server's Rest method will probably return something
            // other than "duckbilledPlatypi" in your case
            request.Resource = requestResource + "/" + password + "?resetCode=" + resetCode + "&email=" + email;
            request.Method = method;

            var response = client.Execute(request) as RestResponse;

            if (response != null
                && ((response.StatusCode == HttpStatusCode.OK)
                    && (response.ResponseStatus == ResponseStatus.Completed)))
            // It's probably not necessary to test both
            {
                return true;
            }
            else
                if (response != null)
                {
                    return false;
                }

            return false;
        }

        public static bool CheckInvalidPassword(string requestResource, Method method,
           int userId, string password)
        {
            var client = new RestClient();
            // This, of course, needs to be altered to match the
            // IP Address/Port of the server to which you are connecting
            client.BaseUrl = new Uri(ClientBase);
            var request = new RestRequest();
            // The server's Rest method will probably return something
            // other than "duckbilledPlatypi" in your case
            request.Resource = requestResource + "/" + userId + Seperator + password;
            request.Method = method;

            var response = client.Execute(request) as RestResponse;

            if (response != null
                && ((response.StatusCode == HttpStatusCode.OK)
                    && (response.ResponseStatus == ResponseStatus.Completed)))
            // It's probably not necessary to test both
            {
                return true;
            }
            else
                if (response != null)
                {
                    return false;
                }

            return false;
        }

        public static bool UpdateUserInfo(string requestResource, Method method, UserApi userInfo)
        {
            var client = new RestClient();
            // This, of course, needs to be altered to match the
            // IP Address/Port of the server to which you are connecting
            client.BaseUrl = new Uri(ClientBase);
            var request = new RestRequest();
            // The server's Rest method will probably return something
            // other than "duckbilledPlatypi" in your case
            request.Resource = requestResource + "/" 
                + userInfo.Id + Seperator 
                + userInfo.Password + Seperator 
                + " " + Seperator
                + userInfo.LastName + Seperator
                + userInfo.FirstName + Seperator
                + userInfo.Phone
                + "?email=" + userInfo.Email;
            request.Method = method;

            var response = client.Execute(request) as RestResponse;

            if (response != null
                && ((response.StatusCode == HttpStatusCode.OK)
                    && (response.ResponseStatus == ResponseStatus.Completed)))
            // It's probably not necessary to test both
            {
                return true;
            }
            else
                if (response != null)
                {
                    return false;
                }

            return false;
        }

        public static bool DeleteAFollowedProduct(string requestResource, Method method, 
            int userId, int fpId)
        {
            var client = new RestClient();
            // This, of course, needs to be altered to match the
            // IP Address/Port of the server to which you are connecting
            client.BaseUrl = new Uri(ClientBase);
            var request = new RestRequest();
            // The server's Rest method will probably return something
            // other than "duckbilledPlatypi" in your case
            request.Resource = requestResource + "/" + userId + Seperator + fpId;
            request.Method = method;

            var response = client.Execute(request) as RestResponse;

            if (response != null
                && ((response.StatusCode == HttpStatusCode.OK)
                    && (response.ResponseStatus == ResponseStatus.Completed)))
            // It's probably not necessary to test both
            {
                return true;
            }
            else
                if (response != null)
                {
                    return false;
                }

            return false;
        }

        public static bool UploadAccountImage(string requestResource, Method method, 
            int userId, string content)
        {
            var client = new RestClient();
            // This, of course, needs to be altered to match the
            // IP Address/Port of the server to which you are connecting
            client.BaseUrl = new Uri(ClientBase);
            var request = new RestRequest();
            // The server's Rest method will probably return something
            // other than "duckbilledPlatypi" in your case
            request.Resource = requestResource + "/" + userId + Seperator + content;
            request.Method = method;

            var response = client.Execute(request) as RestResponse;

            if (response != null
                && ((response.StatusCode == HttpStatusCode.OK)
                    && (response.ResponseStatus == ResponseStatus.Completed)))
            // It's probably not necessary to test both
            {
                return true;
            }
            else
                if (response != null)
                {
                    return false;
                }

            return false;
        }
    }
}
