﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using RestSharp;

namespace TraoDoiDoCu_FrontendMVC.Controllers.Dashboard
{
    public class DashboardController : Controller
    {
        public ActionResult Index()
        {
            int i = 1;
            ViewBag.IsActive = i;
            ViewBag.IsActive_child = 1;
            return View("Dashboard");
        }

        // GET: Dashboard
        public ActionResult ThongBao()
        {
            int i = 1;
            ViewBag.IsActive = i;
            ViewBag.IsActive_child = 2;
            return View("ThongBao");

        }

        [AcceptVerbs("POST")]
        public ActionResult ThongBao(string noiDungThongBao, string nguoiNhan, FormCollection form)
        {
            if (nguoiNhan != null)
            {
                // Code gui tin nhan
                return RedirectToAction("Index", "Dashboard");
            }
            else
            {
                WebApiHelper.Request("api/dashboard/add/" + noiDungThongBao, Method.PUT);
                return RedirectToAction("Index", "Dashboard");
            }
        }

        public ActionResult XuLyTaiKhoan()
        {
            int i = 1;
            ViewBag.IsActive = i;
            ViewBag.IsActive_child = 1;
            return View("XuLyTaiKhoan");
        }

        public ActionResult BlockUser(string listUser)
        {
            string html_result = "Khóa Thành Công";
            WebApiHelper.Request("api/dashboard/blockUser/" + listUser, Method.POST);
            return Content(html_result, "text/plain");
        }

        public ActionResult DeleteUser(string listUser)
        {
            string html_result = "Xóa Thành Công";
            WebApiHelper.Request("api/dashboard/deleteUser/" + listUser, Method.DELETE);
            return Content(html_result, "text/plain");
        }

        public ActionResult CheckBanAccount()
        {
            WebApiHelper.Request("api/dashboard/checkBanAccount", Method.POST);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Search(string term)
        {
            List<string> users = WebApiHelper.GetStringsFromServer("api/dashboard/search/" + term, Method.GET);
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchUser(string username)
        {
            List<String> results = WebApiHelper.GetStringsFromServer("api/dashboard/search/" + username, Method.GET);
            String htmlResult = "";

            foreach (String result in results)
            {
                htmlResult += "<div class=\"row search_user_result\">";
                htmlResult += "<div class=\"col-md-9\">";
                htmlResult += "<div class=\"input-group\">";
                htmlResult += "<span class=\"input-group-addon\">";
                htmlResult += "<input id=\"" + result + "\" type=\"checkbox\" aria-label=\"...\">";
                htmlResult += "</span>";
                htmlResult += "<input id=\"test1\" type=\"text\" disabled style=\"width: 400px\" value=\"" 
                    + result + "\" class=\"form-control\" aria-label=\"...\">";
                htmlResult += "</div>";
                htmlResult += "</div>";
                htmlResult += "</div>";
            }
            htmlResult += "<br />";
            htmlResult += "<div class=\"row search_user_result\">";
            htmlResult += "<div class=\"col-md-8 col-md-offset-1\">";
            htmlResult += "<button type=\"button\" id=\"khoataikhoan\" class=\"btn btn-warning btn-xulytk\">" 
                + "Khóa Tài Khoản</button>";
            htmlResult += "<button type=\"button\" id=\"xoataikhoan\" class=\"btn btn-danger btn-xulytk\">" 
                + "Xóa Tài Khoản</button>";
            htmlResult += "<button type=\"button\" class=\"btn btn-info btn-xulytk\">Hủy Bỏ</button>";
            htmlResult += " </div>";
            htmlResult += "</div>";

            return Content(htmlResult, "text/plain");
        }

        public ActionResult Statistics()
        {
            int slAcc = (from p in WebApiHelper.GetUsersFromServer("api/accountManager/account/all", Method.GET)
                          select p).Count();
            ViewBag.SL_acc = slAcc;
            var now = DateTime.Now;
            int slPost = (from p in WebApiHelper.GetProductsFromServer("api/product/all", Method.GET)
                           where (p.PostingDate.Month == now.Month &&
                                  p.PostingDate.Year == now.Year)
                           select p).Count();
            ViewBag.SL_post = slPost;

            int slMess = (from p in WebApiHelper.GetMessagesFromServer("api/userAction/message/all", Method.GET)
                           where (p.DatePost.Month == now.Month &&
                                  p.DatePost.Year == now.Year)
                           select p).Count();
            ViewBag.SL_mess = slMess;
            ViewBag.IsActive = 2;
            ViewBag.IsActive_child = 3;
            return View();
        }
        public ActionResult Sta_post(int year)
        {
            var ps = WebApiHelper.GetProductsFromServer("pi/product/all", Method.GET);

            Dictionary<String, Int32> info = new Dictionary<string, int>();
            int slPost;
            if (year == 0)
            {
                slPost = (from p in ps
                           select p).Count();
                ViewBag.SSL_post = slPost;

            }
            else
            {
                slPost = (from p in ps
                           where p.PostingDate.Year <= year
                           select p
                             ).Count();
                ViewBag.SSL_post = slPost;
            }
            if (slPost == 0) slPost = 1;
            for (int i = 1; i <= 12; i++)
            {
                int sll = (from p in ps
                           where (p.PostingDate.Month == i
                                  && p.PostingDate.Year == year)
                           select p).Count();

                int temp = sll * 80 / slPost;
                if (i == 1) info.Add("Thang 1", temp);
                if (i == 2) info.Add("Thang 2", temp);
                if (i == 3) info.Add("Thang 3", temp);
                if (i == 4) info.Add("Thang 4", temp);
                if (i == 5) info.Add("Thang 5", temp);
                if (i == 6) info.Add("Thang 6", temp);
                if (i == 7) info.Add("Thang 7", temp);
                if (i == 8) info.Add("Thang 8", temp);
                if (i == 9) info.Add("Thang 9", temp);
                if (i == 10) info.Add("Thang 10", temp);
                if (i == 11) info.Add("Thang 11", temp);
                if (i == 12) info.Add("Thang 12", temp);
            }
            ViewBag.Chart_post = info;
            ViewBag.IsActive = 2;
            ViewBag.IsActive_child = 5;
            return View();






        }
        [HttpPost]
        public ActionResult Sta_post(string year)
        {
            var ps = WebApiHelper.GetProductsFromServer("api/product/all", Method.GET);

            Dictionary<String, Int32> info = new Dictionary<string, int>();
            int nYear = Int32.Parse(year);
            int slPost;
            if (nYear == 0)
            {
                slPost = (from p in ps
                           select p).Count();
                ViewBag.SSL_post = slPost;

            }
            else
            {
                slPost = (from p in ps
                           where p.PostingDate.Year <= nYear
                           select p
                             ).Count();
                ViewBag.SSL_post = slPost;
            }

            if (slPost == 0) slPost = 1;
            for (int i = 1; i <= 12; i++)
            {
                int sll = (from p in ps
                           where (p.PostingDate.Month == i
                                  && p.PostingDate.Year == nYear)
                           select p).Count();

                int temp = sll * 80 / slPost;
                if (i == 1) info.Add("Thang 1", temp);
                if (i == 2) info.Add("Thang 2", temp);
                if (i == 3) info.Add("Thang 3", temp);
                if (i == 4) info.Add("Thang 4", temp);
                if (i == 5) info.Add("Thang 5", temp);
                if (i == 6) info.Add("Thang 6", temp);
                if (i == 7) info.Add("Thang 7", temp);
                if (i == 8) info.Add("Thang 8", temp);
                if (i == 9) info.Add("Thang 9", temp);
                if (i == 10) info.Add("Thang 10", temp);
                if (i == 11) info.Add("Thang 11", temp);
                if (i == 12) info.Add("Thang 12", temp);
            }
            ViewBag.Chart_post = info;
            ViewBag.IsActive = 2;
            ViewBag.IsActive_child = 5;
            return View();


        }
        [HttpGet]
        public ActionResult Sta_acc(int year)
        {
            var users = WebApiHelper.GetUsersFromServer("api/accountManager/account/all", Method.GET);

            Dictionary<String, Int32> info = new Dictionary<string, Int32>();
            if (year == 0)
            {
                int slAcc = (from p in users
                              select p).Count();
                ViewBag.SSL_acc = slAcc;
                int slAdmin = (from p in users
                                where p.Admin == true
                                select p).Count();
                ViewBag.SSL_admin = slAdmin;
                int slAccbaned = (from p in users
                                   where p.Ban != null
                                   select p).Count();
                ViewBag.SSL_accbaned = slAccbaned;



            }
            else
            {
                int slAcc = (from p in users
                              where p.DateRequest != null && p.DateRequest.Value.Year <= year
                              select p
                              ).Count();
                ViewBag.SSL_acc = slAcc;
                int slAdmin = (from p in users
                                where p.DateRequest != null && (p.Admin == true && p.DateRequest.Value.Year <= year)
                                select p).Count();
                ViewBag.SSL_admin = slAdmin;
                int slAccbaned = (from p in users
                                   where p.DateRequest != null && (p.Ban != null && p.DateRequest.Value.Year <= year)
                                   select p).Count();
                ViewBag.SSL_accbaned = slAccbaned;
            }

            int slAcc1 = (from p in users
                           where p.DateRequest != null && p.DateRequest.Value.Year == DateTime.Now.Year
                           select p
                             ).Count();
            if (slAcc1 == 0) slAcc1 = 1;
            for (int i = 1; i <= 12; i++)
            {
                int sll = (from p in users
                           where p.DateRequest != null && (p.DateRequest.Value.Month == i
                                                           && p.DateRequest.Value.Year == DateTime.Now.Year)
                           select p).Count();
                int temp = sll * 80 / slAcc1;
                if (i == 1) info.Add("Thang 1", temp);
                if (i == 2) info.Add("Thang 2", temp);
                if (i == 3) info.Add("Thang 3", temp);
                if (i == 4) info.Add("Thang 4", temp);
                if (i == 5) info.Add("Thang 5", temp);
                if (i == 6) info.Add("Thang 6", temp);
                if (i == 7) info.Add("Thang 7", temp);
                if (i == 8) info.Add("Thang 8", temp);
                if (i == 9) info.Add("Thang 9", temp);
                if (i == 10) info.Add("Thang 10", temp);
                if (i == 11) info.Add("Thang 11", temp);
                if (i == 12) info.Add("Thang 12", temp);
            }
            ViewBag.Chart_acc = info;
            ViewBag.IsActive = 2;
            ViewBag.IsActive_child = 4;
            return View();

        }

        [HttpPost]
        public ActionResult Sta_acc(string year)
        {
            Dictionary<String, Int32> info = new Dictionary<string, int>();
            //tmp = info.Keys.ToList();
            //info[tmp[i]] = 0;
            var users = WebApiHelper.GetUsersFromServer("api/accountManager/account/all", Method.GET);


            int nYear = Int32.Parse(year);


            int slAcc = (from p in users
                          where p.DateRequest != null && p.DateRequest.Value.Year <= nYear
                          select p
                          ).Count();
            ViewBag.SSL_acc = slAcc;
            int slAdmin = (from p in users
                            where p.DateRequest != null && (p.Admin == true && p.DateRequest.Value.Year <= nYear)
                            select p).Count();
            ViewBag.SSL_admin = slAdmin;
            int slAccbaned = (from p in users
                               where p.DateRequest != null && (p.Ban != null && p.DateRequest.Value.Year <= nYear)
                               select p).Count();
            ViewBag.SSL_accbaned = slAccbaned;
            if (slAcc == 0) slAcc = 1;
            for (int i = 1; i <= 12; i++)
            {
                int sll = (from p in users
                           where p.DateRequest != null && (p.DateRequest.Value.Month == i
                                                           && p.DateRequest.Value.Year == nYear)
                           select p).Count();

                int temp = sll * 80 / slAcc;
                if (i == 1) info.Add("Thang 1", temp);
                if (i == 2) info.Add("Thang 2", temp);
                if (i == 3) info.Add("Thang 3", temp);
                if (i == 4) info.Add("Thang 4", temp);
                if (i == 5) info.Add("Thang 5", temp);
                if (i == 6) info.Add("Thang 6", temp);
                if (i == 7) info.Add("Thang 7", temp);
                if (i == 8) info.Add("Thang 8", temp);
                if (i == 9) info.Add("Thang 9", temp);
                if (i == 10) info.Add("Thang 10", temp);
                if (i == 11) info.Add("Thang 11", temp);
                if (i == 12) info.Add("Thang 12", temp);
            }
            ViewBag.Chart_acc = info;
            ViewBag.IsActive = 2;
            ViewBag.IsActive_child = 4;
            return View();


        }
        public ActionResult Sta_mess(int year)
        {
            var ms = WebApiHelper.GetMessagesFromServer("api/userAction/message/all", Method.GET);

            Dictionary<String, Int32> info = new Dictionary<string, int>();
            int slMess;
            if (year == 0)
            {
                slMess = (from p in ms
                           select p).Count();
                ViewBag.SSL_mess = slMess;

            }
            else
            {
                slMess = (from p in ms
                           where p.DatePost.Year <= year
                           select p
                              ).Count();
                ViewBag.SSL_post = slMess;
            }

            if (slMess == 0) slMess = 1;
            for (int i = 1; i <= 12; i++)
            {
                int sll = (from p in ms
                           where (p.DatePost.Month == i
                                  && p.DatePost.Year == year)
                           select p).Count();

                int temp = sll * 80 / slMess;
                if (i == 1) info.Add("Thang 1", temp);
                if (i == 2) info.Add("Thang 2", temp);
                if (i == 3) info.Add("Thang 3", temp);
                if (i == 4) info.Add("Thang 4", temp);
                if (i == 5) info.Add("Thang 5", temp);
                if (i == 6) info.Add("Thang 6", temp);
                if (i == 7) info.Add("Thang 7", temp);
                if (i == 8) info.Add("Thang 8", temp);
                if (i == 9) info.Add("Thang 9", temp);
                if (i == 10) info.Add("Thang 10", temp);
                if (i == 11) info.Add("Thang 11", temp);
                if (i == 12) info.Add("Thang 12", temp);
            }
            ViewBag.Chart_mess = info;
            ViewBag.IsActive = 2;
            ViewBag.IsActive_child = 6;
            return View();
        }
        [HttpPost]
        public ActionResult Sta_mess(string year)
        {
            var ms = WebApiHelper.GetMessagesFromServer("api/userAction/message/all", Method.GET);

            var info = new Dictionary<string, int>();
            int slMess;
            int nYear = Int32.Parse(year);

            if (nYear == 0)
            {
                slMess = (from p in ms
                           select p).Count();
                ViewBag.SSL_mess = slMess;

            }
            else
            {
                slMess = (from p in ms
                           where p.DatePost.Year <= nYear
                           select p
                              ).Count();
                ViewBag.SSL_mess = slMess;
            }

            if (slMess == 0) slMess = 1;
            for (int i = 1; i <= 12; i++)
            {
                int sll = (from p in ms
                           where (p.DatePost.Month == i
                                  && p.DatePost.Year == nYear)
                           select p).Count();

                int temp = sll * 80 / slMess;
                if (i == 1) info.Add("Thang 1", temp);
                if (i == 2) info.Add("Thang 2", temp);
                if (i == 3) info.Add("Thang 3", temp);
                if (i == 4) info.Add("Thang 4", temp);
                if (i == 5) info.Add("Thang 5", temp);
                if (i == 6) info.Add("Thang 6", temp);
                if (i == 7) info.Add("Thang 7", temp);
                if (i == 8) info.Add("Thang 8", temp);
                if (i == 9) info.Add("Thang 9", temp);
                if (i == 10) info.Add("Thang 10", temp);
                if (i == 11) info.Add("Thang 11", temp);
                if (i == 12) info.Add("Thang 12", temp);
            }
            ViewBag.Chart_mess = info;
            ViewBag.IsActive = 2;
            ViewBag.IsActive_child = 6;
            return View();
        }

    }
}