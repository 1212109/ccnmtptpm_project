﻿using System.Collections.Generic;
using System.Linq;
using DataTransferLayer.Products;

namespace DataAccessLayer
{
    public class HomeDao
    {
        private static HomeDao _instance;

        public static HomeDao Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new HomeDao();
                }

                return _instance;
            }
        }

        private readonly DatabaseContexts _db;

        private HomeDao()
        {
            _db = DatabaseContexts.Instance;
        }

        public int GetCatIdFromCatName(string name)
        {
            foreach (var c in _db.Categories)
            {
                if (c.Name == name)
                    return c.Id;
            }

            return -1;
        }

        public List<Product> GetProductFromCatId(int id)
        {
            var products = from p in _db.Products
                           where p.CategoryId == id
                           select p;

            return products.ToList();
        }

        public int GetPosIdFromPosName(string pos)
        {
            foreach (var c in _db.Cities)
            {
                if (c.Name == pos)
                    return c.Id;
            }

            return -1;
        }
    }
}
