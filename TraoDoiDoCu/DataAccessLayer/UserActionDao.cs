﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataTransferLayer.Accounts;

namespace DataAccessLayer
{
    public class UserActionDao
    {
        private static UserActionDao _instance;

        public static UserActionDao Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new UserActionDao();
                }

                return _instance;
            }
        }

        private readonly DatabaseContexts _db;

        private UserActionDao()
        {
            _db = DatabaseContexts.Instance;
        }

        public List<Comment> GetComments()
        {
            return _db.Comments.ToList();
        }

        public List<Message> GetMessages()
        {
            return _db.Messages.ToList();
        } 

        public List<VoteLog> GetVoteLogs()
        {
            return _db.VoteLogs.ToList();
        } 

        public bool AddComment(string username, int productId, string desc)
        {
            Comment comment = new Comment();

            comment.PostingDate = DateTime.Now;
            comment.ProductId = productId;
            comment.TextContent = desc;

            var u = (from d in _db.Users
                     where d.UserName == username
                     select new { d.Id }).SingleOrDefault();

            if (u != null)
            {
                comment.UserId = u.Id;

                _db.Comments.Add(comment);
                _db.SaveChangesAsync();

                return true;
            }

            return false;
        }

        public bool CheckFollow(string username, int productId)
        {           
            User target = _db.Users.FirstOrDefault(x => x.UserName == username);

            if (target != null)
            {
                FollowProduct fl = new FollowProduct();

                fl.UserId = target.Id;
                fl.ProductId = productId;
                fl.Active = true;

                _db.FollowProducts.Add(fl);
                _db.SaveChanges();

                return true;
            }

            return false;
        }

        public bool UnFollow(string username, int productId)
        {
            User target = _db.Users.FirstOrDefault(x => x.UserName == username);

            if (target != null)
            {
                List<FollowProduct> fl = _db.FollowProducts.
                    Where(x => x.ProductId == productId && x.UserId == target.Id).ToList();

                foreach (FollowProduct follow in fl)
                {
                    follow.Active = false;
                    _db.FollowProducts.Remove(follow);
                }

                _db.SaveChanges();

                return true;
            }          

            return false;
        }

        public bool SendRating(int sectionId, string username, int autoId, int thisVote)
        {
            var poster = _db.Users.Where(sc => sc.Id == autoId).FirstOrDefault();
            var voteCount = _db.VoteLogs.Where(vc => vc.VoteForId == autoId).Count();

            if (poster != null)
            {
                int curvote = 0;

                if (voteCount > 0)
                    curvote = (curvote*voteCount + thisVote)/(voteCount + 1);
                else 
                    if (voteCount == 0)
                        curvote = thisVote;

                poster.Rating = curvote;
                _db.SaveChanges();

                VoteLog vm = new VoteLog()
                {
                    Active = true,
                    SectionId = sectionId,
                    UserName = username,
                    Vote = thisVote,
                    VoteForId = autoId
                };

                _db.VoteLogs.Add(vm);
                _db.SaveChanges();

                return true;
            }

            return false;
        }

        public bool ReportPost(string username, int reportForId, int productId, 
            string reasonP, string reasonU, string desc)
        {
            var u = (from d in _db.Users
                     where d.UserName == username
                     select new { d.Id }).SingleOrDefault();

            var isIt = _db.Reports.Where(r => r.ReportForId == reportForId 
                && r.ProductId == productId 
                && r.UserId == u.Id).FirstOrDefault();

            //Chua report
            if (isIt == null && u != null)
            {              
                Report r = new Report()
                {
                    UserId = u.Id,
                    ReportForId = reportForId,
                    ProductId = productId,
                    ReasonByProduct = reasonP,
                    ReasonByUser = reasonU,
                    Description = desc
                };

                _db.Reports.Add(r);              
                _db.SaveChanges();

                return true;
            }

            return false;
        }

        public void SendMessage(string content, int senderId, int receiverId)
        {
            Message mess = new Message();
            mess.Content = content;
            mess.SenderId = senderId;
            mess.ReceiverId = receiverId;

            _db.Messages.Add(mess);
            _db.SaveChangesAsync();
        }       

        public void BanAccount(List<string> usernames)
        {
            foreach (User a in _db.Users)
            {
                if (usernames.Contains(a.UserName))
                {
                    a.Ban = true;
                    a.BanDate = DateTime.Now;
                    a.BanTime = 30;
                }

            }
            
            _db.SaveChangesAsync();
        }

        public void CheckBanAccount()
        {
            foreach (User a in _db.Users)
            {
                if (a.BanDate != null && DateTime.Compare(DateTime.Now, (DateTime)a.BanDate) > 30)
                {
                    a.Ban = false;
                    a.BanDate = DateTime.Now;
                    a.BanTime = 0;
                }
            }

            _db.SaveChangesAsync();
        }
    }
}
