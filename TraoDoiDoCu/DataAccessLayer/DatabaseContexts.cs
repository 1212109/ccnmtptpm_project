﻿using System.Data.Entity;
using DataTransferLayer;
using DataTransferLayer.Accounts;
using DataTransferLayer.Products;

namespace DataAccessLayer
{
    public class DatabaseContexts : DbContext
    {
        private static DatabaseContexts _instance;

        public static DatabaseContexts Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DatabaseContexts();
                }

                return _instance;
            }
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<FollowProduct> FollowProducts { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<VoteLog> VoteLogs { get; set; }

        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<City> Cities { get; set; }         
        public DbSet<ProductImage> ProductImages { get; set; }

        public DbSet<GlobalData> GlobalDatas { get; set; }

        private DatabaseContexts()
        {
            Database.Connection.
                ConnectionString = "Data Source=.;Initial Catalog=TraoDoiDoCu_New;Integrated Security=true";

            //var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Users
            modelBuilder.Entity<User>().HasMany(u => u.Comments).WithRequired(c => c.User).
                HasForeignKey(c => c.UserId).WillCascadeOnDelete(true);
            modelBuilder.Entity<User>().HasMany(u => u.FollowProducts).WithRequired(fp => fp.User).
               HasForeignKey(fp => fp.UserId).WillCascadeOnDelete(true);
            modelBuilder.Entity<User>().HasMany(u => u.SentMessages).WithRequired(m => m.Sender).
                HasForeignKey(m => m.SenderId).WillCascadeOnDelete(true);
            modelBuilder.Entity<User>().HasMany(u => u.ReceivedMessages).WithRequired(m => m.Receiver).
                HasForeignKey(m => m.ReceiverId).WillCascadeOnDelete(false);
            modelBuilder.Entity<User>().HasMany(u => u.Products);
            modelBuilder.Entity<User>().HasMany(u => u.Reports).WithRequired(r => r.User).
                HasForeignKey(r => r.UserId).WillCascadeOnDelete(true);
            modelBuilder.Entity<User>().HasMany(u => u.Reports1).WithRequired(r => r.UserFor).
                HasForeignKey(r => r.ReportForId).WillCascadeOnDelete(false);                  
            modelBuilder.Entity<User>().HasMany(u => u.VoteLogs).WithRequired(v => v.User).
                HasForeignKey(v => v.VoteForId).WillCascadeOnDelete(true);

            //Catalogs
            modelBuilder.Entity<Category>().HasMany(c => c.Products).WithRequired(p => p.Category).
                HasForeignKey(p => p.CategoryId).WillCascadeOnDelete(true);

            //Cities
            modelBuilder.Entity<City>().HasMany(c => c.Products).WithRequired(p => p.City).
                HasForeignKey(p => p.Location).WillCascadeOnDelete(true);

            //Products
            modelBuilder.Entity<Product>().HasMany(p => p.Comments).WithRequired(c => c.Product).
                HasForeignKey(c => c.ProductId).WillCascadeOnDelete(false);
            modelBuilder.Entity<Product>().HasMany(p => p.FollowProducts).WithRequired(f => f.Product).
                HasForeignKey(f => f.ProductId).WillCascadeOnDelete(false);
            modelBuilder.Entity<Product>().HasMany(p => p.ProductImages).WithRequired(pi => pi.Product).
                HasForeignKey(pi => pi.ProductId).WillCascadeOnDelete(true);
            modelBuilder.Entity<Product>().HasMany(p => p.Reports).WithRequired(r => r.Product).
                HasForeignKey(r => r.ProductId).WillCascadeOnDelete(false);

            base.OnModelCreating(modelBuilder);
        }
    }
}
