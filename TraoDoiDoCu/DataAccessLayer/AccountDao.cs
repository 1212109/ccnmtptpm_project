﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using DataTransferLayer.Accounts;
using DataTransferLayer.Products;

namespace DataAccessLayer
{
    public class AccountDao
    {
        private static AccountDao _instance;

        public static AccountDao Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new AccountDao();
                }

                return _instance;
            }
        }

        private readonly DatabaseContexts _db;

        public static string Hash(string password)
        {
            var hash = Encoding.ASCII.GetBytes("san" + password);
            var sha1 = new SHA1CryptoServiceProvider();
            byte[] sha1Hash = sha1.ComputeHash(hash);
            var hashedPassword = Encoding.ASCII.GetString(sha1Hash);
            return hashedPassword;
        }

        private void InitializeData()
        {
            if (_db.Users.ToList().Count == 0)
            {
                User u =  new User();
                u.UserName = "admin";
                u.Password = Hash("123456");
                u.ActiveCode = "active";
                u.Admin = true;
                u.Email = "superadmin@supermail.com";
                u.FirstName = "Admin";
                u.LastName = "Super";
                u.Phone = "0123456789";
                _db.Users.Add(u);

                u = new User();
                u.UserName = "user01";
                u.Password = Hash("123456");
                u.ActiveCode = "active";
                u.Admin = false;
                u.Email = "user01@supermail.com";
                u.FirstName = "User";
                u.LastName = "01";
                u.Phone = "01234567891";
                _db.Users.Add(u);

                u = new User();
                u.UserName = "user02";
                u.Password = Hash("123456");
                u.ActiveCode = "active";
                u.Admin = false;
                u.Email = "user02@supermail.com";
                u.FirstName = "User";
                u.LastName = "02";
                u.Phone = "01234567892";
                _db.Users.Add(u);

                u = new User();
                u.UserName = "user03";
                u.Password = Hash("123456");
                u.ActiveCode = "active";
                u.Admin = false;
                u.Email = "user03@supermail.com";
                u.FirstName = "User";
                u.LastName = "03";
                u.Phone = "01234567893";
                _db.Users.Add(u);

                _db.SaveChanges();
            }
        }

        private AccountDao()
        {
            _db = DatabaseContexts.Instance;

            //InitializeData();
        }

        public List<User> GetUsers()
        {
            return _db.Users.ToList();
        }

        public User GetUser(int userId)
        {
            var u = _db.Users.Where(x => x.Id == userId);
            var u2 = u.ToList();
            if (u2.Count > 0)
            {
                return u2[0];
            }

            return null;
        }

        public User GetUser(string username)
        {
            List<User> exists = _db.Users.Where(x => x.UserName == username).ToList();
            if (exists.Count > 0)
            {
                return exists[0];
            }

            return null;
        } 

        public List<string> SearchUsernames(string content)
        {
            return _db.Users.Where(p => p.UserName.Contains(content)).Select(p => p.UserName).ToList();
        }

        public bool DeleteUsers(List<string> usernames)
        {
            if (usernames != null)
            {
                List<User> users = new List<User>();
                foreach (User a in _db.Users)
                {
                    if (usernames.Contains(a.UserName))
                        users.Add(a);
                }

                foreach (User a in users)
                    _db.Users.Remove(a);

                _db.SaveChanges();
            }           

            return false;
        }

        #region # phần login
        public bool LoginIsValid(string userName, string hashPassword)
        {
            User user = _db.Users.FirstOrDefault(x => x.UserName == userName && x.Password == hashPassword);
            if (user != null)
                return true;
            return false;
        }

        public bool IsActivedAccount(string userName)
        {
            User user = _db.Users.FirstOrDefault(x => x.UserName == userName);
            if (user == null)
                return false;
            if (user.ActiveCode == "active")
                return true;
            return false;
        }

        #endregion

        #region # phần register
        public bool CheckExistenceAccount(string email, string userName)
        {
            var user = _db.Users.Where(x => x.UserName == userName || x.Email == email).ToList();
            if (user.Count > 0)
            {
                return true;
            }

            return false;
        }

        public string AddAccount(User user)
        {
            User newUser = new User();
            newUser.UserName = user.UserName;
            newUser.Password = user.Password;
            newUser.Email = user.Email;
            newUser.Admin = false;
            newUser.FirstName = user.FirstName;
            newUser.LastName = user.LastName;
            newUser.Phone = user.Phone;
            newUser.Rating = 0;
            newUser.DateRequest = DateTime.Now;
            newUser.UserImage = "avatar.png";

            newUser.Products = null;

            string activationCode = Guid.NewGuid().ToString();
            newUser.ActiveCode = activationCode;

            try
            {
                _db.Users.Add(newUser);
                _db.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity,
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                return null;
            }
            return activationCode;
        }

        #endregion

        #region # phần active account
        public bool ActivateAccount(string userName, string activationCode)
        {
            User user = _db.Users.FirstOrDefault(x => x.UserName == userName && x.ActiveCode == activationCode);
            if (user?.UserName == null)
                return false;

            user.ActiveCode = "active";
            _db.SaveChanges();
            return true;
        }
        #endregion

        //private float TIME_OVERDUE_RESET_PASSWORD = 2;
        #region # phần forgot password
        public bool CheckMailExistence(string email)
        {
            User user = _db.Users.FirstOrDefault(x => x.Email == email);
            if (user != null)
                return true;
            return false;
        }

        public string SetUpResetPassword(string email)
        {
            User user = _db.Users.FirstOrDefault(x => x.Email == email);
            if (user == null)
            {
                return null;
            }

            string resetPassCode = Guid.NewGuid().ToString();
            user.ResetPassword = resetPassCode;
            DateTime time = DateTime.Now;
            user.DateRequest = time;

            try
            {
                _db.SaveChanges();
            }
            catch (Exception)
            {
                return null;
            }
            return user.ResetPassword;
        }

        public bool CheckResetPassword(string email, string resetCode)
        {
            User user = _db.Users.FirstOrDefault(x => x.Email == email && x.ResetPassword == resetCode);
            if (user != null)
            {
                /*DateTime now = DateTime.Now;
                DateTime time = user.DateRequest.Value.ToLocalTime();
                TimeSpan span = now.Subtract(time);
                if (span.Hours > TIME_OVERDUE_RESET_PASSWORD)
                    return false;
                else*/
                return true;
            }
            return false;
        }

        public bool ResetPassword(ResetPasswordDto resetPassVm)
        {
            User user = _db.Users.
                FirstOrDefault(x => x.Email == resetPassVm.Email 
                && x.ResetPassword == resetPassVm.ResetCode);
            if (user != null)
            {
                try
                {
                    var hash = Encoding.ASCII.GetBytes("san" + resetPassVm.Password);
                    var sha1 = new SHA1CryptoServiceProvider();
                    byte[] sha1Hash = sha1.ComputeHash(hash);
                    var hashedPassword = Encoding.ASCII.GetString(sha1Hash);

                    user.Password = hashedPassword;
                    _db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

        #endregion

        #region phần Get Sender Infomation
        public User GetUserInfo(int userId)
        {
            User result;
            result = _db.Users.FirstOrDefault(x => x.Id == userId);
            return result;

        }

        #endregion

        #region phần Update Sender Info
        public bool UpdateUserInfo(int userId, User newUserInfo)
        {
            User target = _db.Users.FirstOrDefault(x => x.Id == userId);
            if (target != null)
            {
                if (newUserInfo.Password.Count() > 0)
                    target.Password = Hash(newUserInfo.Password);
                if (newUserInfo.FirstName.Count() > 0)
                    target.FirstName = newUserInfo.FirstName;
                if (newUserInfo.LastName.Count() > 0)
                    target.LastName = newUserInfo.LastName;
                if (newUserInfo.Phone.Count() > 0)
                    target.Phone = newUserInfo.Phone;
            }

            _db.Users.Attach(target);
            var entry = _db.Entry(target);
            entry.Property(x => x.Password).IsModified = true;
            entry.Property(x => x.FirstName).IsModified = true;
            entry.Property(x => x.LastName).IsModified = true;
            entry.Property(x => x.Phone).IsModified = true;

            _db.SaveChanges();
            return true;
        }
        #endregion

        #region phần Delete Sender
        public bool DeleteUserAccount(int userId)
        {
            User target = _db.Users.FirstOrDefault(x => x.Id == userId);
            try
            {
                if (target != null)
                {
                    target.Ban = true;
                    _db.Users.Attach(target);
                    var entry = _db.Entry(target);
                    entry.Property(x => x.Ban).IsModified = true;
                }
                _db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }



        }

        #endregion
        public bool CheckInvalidPassword(int userId, string hashedPassword)
        {
            User user = _db.Users.FirstOrDefault(x => x.Id == userId && x.Password == hashedPassword);
            if (user != null)
                return true;
            return false;
        }

        public int GetIdFromUsername(string username)
        {
            int result = -1;
            User target = _db.Users.FirstOrDefault(x => x.UserName == username);
            if (target != null)
            {
                result = target.Id;
            }
            return result;
        }


        #region phần show followed products

        //get list Id of followed products
        public List<int> GetIdProductsFollowed(string userName)
        {
            List<int> result = new List<int>();
            var user = _db.Users.FirstOrDefault(x => x.UserName == userName);
            if (user != null)
            {
                var target = _db.FollowProducts.Where(x => x.UserId == user.Id);
                foreach (FollowProduct t in target)
                {
                    result.Add(t.ProductId);
                }
            }
            return result;
        }

        //get followed product using list of productID
        public List<Product> GetProductsFollowed(List<int> lstOfProductIDs)
        {
            List<Product> result = new List<Product>();
            foreach (int id in lstOfProductIDs)
            {
                Product temp = _db.Products.FirstOrDefault(x => x.Id == id);
                if (temp != null)
                    result.Add(temp);
            }
            return result;
        }

        public bool DeleteAFollowedProduct(int followedProductId, int userId)
        {
            FollowProduct target;
            target = _db.FollowProducts.
                FirstOrDefault(x => x.ProductId == followedProductId && x.UserId == userId);
            if (target != null)
            {
                try
                {
                    _db.FollowProducts.Remove(target);
                    _db.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            return false;
        }
        #endregion

        public bool GetRoleUser(string userName)
        {
            User user = _db.Users.FirstOrDefault(x => x.UserName == userName);
            if (user == null)
                return false;
            if (user.Admin == true)
                return true;
            return false;
        }

        public bool UploadAccountImage(int userId, string fileName)
        {
            bool result = false;

            User user = _db.Users.FirstOrDefault(x => x.Id == userId);
            if (user != null)
            {
                user.UserImage = fileName;
                _db.SaveChanges();
                result = true;
            }

            return result;
        }
    }
}
