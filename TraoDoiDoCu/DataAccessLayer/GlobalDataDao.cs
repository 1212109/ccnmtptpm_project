﻿using System.Collections.Generic;
using DataTransferLayer;

namespace DataAccessLayer
{
    public class GlobalDataDao
    {
        private static GlobalDataDao _instance;

        public static GlobalDataDao Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new GlobalDataDao();
                }

                return _instance;
            }
        }

        private readonly DatabaseContexts _db;

        private GlobalDataDao()
        {
            _db = DatabaseContexts.Instance;
        }

        public string GetNotification()
        {
            foreach (GlobalData globalData in _db.GlobalDatas)
            {
                if (globalData.Name.Equals("ThongBao"))
                {
                    return globalData.Content;
                }
            }

            return null;
        }

        public void AddNotification(string content)
        {

            GlobalData selected = new GlobalData();
            foreach (GlobalData globalData in _db.GlobalDatas)
            {
                if (globalData.Name.Equals("ThongBao"))
                {
                    selected = globalData;
                }
            }

            if (selected.Name != null)
                _db.GlobalDatas.Remove(selected);

            GlobalData newNotification = new GlobalData();
            newNotification.Name = "ThongBao";
            newNotification.Content = content;

            _db.GlobalDatas.Add(newNotification);
            _db.SaveChanges();

        }

        public void DeleteAllNotification()
        {
            List<GlobalData> seletedList = new List<GlobalData>();
            foreach (GlobalData globalData in _db.GlobalDatas)
            {
                if (globalData.Name.Equals("ThongBao"))
                {
                    seletedList.Add(globalData);
                }
            }

            foreach (GlobalData selected in seletedList)
            {
                _db.GlobalDatas.Remove(selected);
            }

            _db.SaveChangesAsync();
        }
    }
}
