﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataTransferLayer.Accounts;
using DataTransferLayer.Products;

namespace DataAccessLayer
{
    public class ProductDao
    {
        private static bool _initialized;

        private static ProductDao _instance;

        public static ProductDao Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ProductDao();
                }

                return _instance;
            }
        }

        private readonly DatabaseContexts _db;

        public static byte[] ReadImageFile(string imageLocation)
        {
            byte[] imageData;
            FileInfo fileInfo = new FileInfo(imageLocation);
            long imageFileLength = fileInfo.Length;
            FileStream fs = new FileStream(imageLocation, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            imageData = br.ReadBytes((int)imageFileLength);
            return imageData;
        }

        public static string ConvertFileToProductImage(string imgPath)
        {
            var bytes = ReadImageFile(imgPath);
            var base64 = Convert.ToBase64String(bytes);
            return string.Format("data:{0};base64,{1}", "image/jpg", base64);
        }

        public bool IniatilazeData(string imgPath)
        {
            try
            {
                if (_initialized == false)
                {
                    _initialized = true;
                }
                else
                {
                    return false;
                }

                if (_db.Users.ToList().Count == 0)
                {
                    User u = new User();
                    u.UserName = "admin";
                    u.Password = AccountDao.Hash("123456");
                    u.ActiveCode = "active";
                    u.Admin = true;
                    u.Email = "superadmin@supermail.com";
                    u.FirstName = "Admin";
                    u.LastName = "Super";
                    u.Phone = "0123456789";
                    _db.Users.Add(u);

                    u = new User();
                    u.UserName = "user01";
                    u.Password = AccountDao.Hash("123456");
                    u.ActiveCode = "active";
                    u.Admin = false;
                    u.Email = "user01@supermail.com";
                    u.FirstName = "User";
                    u.LastName = "01";
                    u.Phone = "01234567891";
                    _db.Users.Add(u);

                    u = new User();
                    u.UserName = "user02";
                    u.Password = AccountDao.Hash("123456");
                    u.ActiveCode = "active";
                    u.Admin = false;
                    u.Email = "user02@supermail.com";
                    u.FirstName = "User";
                    u.LastName = "02";
                    u.Phone = "01234567892";
                    _db.Users.Add(u);

                    u = new User();
                    u.UserName = "user03";
                    u.Password = AccountDao.Hash("123456");
                    u.ActiveCode = "active";
                    u.Admin = false;
                    u.Email = "user03@supermail.com";
                    u.FirstName = "User";
                    u.LastName = "03";
                    u.Phone = "01234567893";
                    _db.Users.Add(u);

                    _db.SaveChanges();
                }

                if (_db.Cities.ToList().Count == 0)
                {
                    City c = new City();
                    c.Name = "Hà Nội";
                    _db.Cities.Add(c);

                    c = new City();
                    c.Name = "Đà Nẵng";
                    _db.Cities.Add(c);

                    c = new City();
                    c.Name = "Hồ Chí Minh";
                    _db.Cities.Add(c);

                    c = new City();
                    c.Name = "Khác";
                    _db.Cities.Add(c);

                    _db.SaveChanges();
                }

                if (_db.Categories.ToList().Count == 0)
                {
                    Category c = new Category();
                    c.Name = "Di động";
                    c.Description = "Điện thoại di động, tablet,..";
                    _db.Categories.Add(c);

                    c = new Category();
                    c.Name = "Đồ điện tử";
                    c.Description = "Đồ điện tử";
                    _db.Categories.Add(c);

                    c = new Category();
                    c.Name = "Dụng cụ bếp";
                    c.Description = "Đồ dùng trong nhà bếp";
                    _db.Categories.Add(c);

                    c = new Category();
                    c.Name = "Đồ chơi";
                    c.Description = "Các loại đồ chơi";
                    _db.Categories.Add(c);

                    c = new Category();
                    c.Name = "Đồ dùng cá nhân";
                    c.Description = "Các loại quần áo thời trang, đồ dùng cá nhân ";
                    _db.Categories.Add(c);

                    c = new Category();
                    c.Name = "Laptop";
                    c.Description = "Các dòng Laptop và các thiết bị laptop";
                    _db.Categories.Add(c);

                    c = new Category();
                    c.Name = "Xe máy";
                    c.Description = "Các loại xe máy";
                    _db.Categories.Add(c);

                    c = new Category();
                    c.Name = "Tivi";
                    c.Description = "Các loại tivi";
                    _db.Categories.Add(c);

                    c = new Category();
                    c.Name = "Khác";
                    c.Description = "Các mặc hàng khác";
                    _db.Categories.Add(c);

                    _db.SaveChanges();
                }

                if (_db.Products.ToList().Count == 0)
                {
                    Product p = new Product();
                    p.UserId = 1;
                    p.Name = "Tu lanh";
                    p.Picture = ConvertFileToProductImage(imgPath + "tu-lanh-sanyo-sr-5kr-sh-mo-ta-chuc-nang.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 3;
                    p.Location = 1;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 2;
                    p.Name = "samsung-galaxy-note-edge-02";
                    p.Picture = ConvertFileToProductImage(imgPath + "samsung-galaxy-note-edge-02.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("1000000");
                    p.ProductKey = "Dien thoai khac";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 1;
                    p.Location = 2;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 3;
                    p.Name = "Xe may";
                    p.Picture = ConvertFileToProductImage(imgPath + "RC_FI_2014_black-20140726-17071132.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("500000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 7;
                    p.Location = 3;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 4;
                    p.Name = "tu-lanh-mini-sanyo";
                    p.Picture = ConvertFileToProductImage(imgPath + "tu-lanh-mini-sanyo.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 3;
                    p.Location = 4;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 1;
                    p.Name = "lg-led-tv-32";
                    p.Picture = ConvertFileToProductImage(imgPath + "lg-led-tv-32.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 8;
                    p.Location = 1;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 2;
                    p.Name = "may tinh bo tui";
                    p.Picture = ConvertFileToProductImage(imgPath 
                        + "3_3_20_1337823145_64_1337768114-may-tinh-casino-4.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 2;
                    p.Location = 2;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 3;
                    p.Name = "Laptop asus";
                    p.Picture = ConvertFileToProductImage(imgPath + "4_asus_k501lb_dm077d_dark_blue.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 6;
                    p.Location = 3;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 4;
                    p.Name = "Quat mini";
                    p.Picture = ConvertFileToProductImage(imgPath + "7300_dool_080806_tk2_8.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 5;
                    p.Location = 4;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 1;
                    p.Name = "Xe dap";
                    p.Picture = ConvertFileToProductImage(imgPath + "29087042081.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 9;
                    p.Location = 1;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 2;
                    p.Name = "Xe do choi";
                    p.Picture = ConvertFileToProductImage(imgPath + "chuot-quang-xe-hoi-ferrari_1281151393.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Cai gi cung duoc";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 4;
                    p.Location = 2;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 3;
                    p.Name = "May anh";
                    p.Picture = ConvertFileToProductImage(imgPath + "D3S_5216-600.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Xe tang";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 9;
                    p.Location = 3;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 4;
                    p.Name = "Bep ga";
                    p.Picture = ConvertFileToProductImage(imgPath + "qkl1384629286.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("220000");
                    p.ProductKey = "Ten lua";
                    p.PostingDate = DateTime.Now;
                    p.Available = false;
                    p.CategoryId = 3;
                    p.Location = 4;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 1;
                    p.Name = "may-lanh-toshiba-ras-h10";
                    p.Picture = ConvertFileToProductImage(imgPath + "may-lanh-toshiba-ras-h10s3ks-v-480x480-6.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 5;
                    p.Location = 1;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 2;
                    p.Name = "lumia-920";
                    p.Picture = ConvertFileToProductImage(imgPath + "lumia-920.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 1;
                    p.Location = 2;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 3;
                    p.Name = "Laptop";
                    p.Picture = ConvertFileToProductImage(imgPath + "30363_k501lb-22.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 6;
                    p.Location = 3;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 4;
                    p.Name = "Xe dap";
                    p.Picture = ConvertFileToProductImage(imgPath + "48_90_mainimage.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("2000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 9;
                    p.Location = 4;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 1;
                    p.Name = "sony-xperia-z5";
                    p.Picture = ConvertFileToProductImage(imgPath + "524522466_sony-xperia-z5-.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("12000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 1;
                    p.Location = 1;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 2;
                    p.Name = "Quat may";
                    p.Picture = ConvertFileToProductImage(imgPath + "351289063617.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("300000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 5;
                    p.Location = 2;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 3;
                    p.Name = "Tu thuoc";
                    p.Picture = ConvertFileToProductImage(imgPath + "medium_oyz1395576346.JPG");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 9;
                    p.Location = 3;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 4;
                    p.Name = "Chuot";
                    p.Picture = ConvertFileToProductImage(imgPath + "full_87667ad7d0bc1498a36cdb3397debf95.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("152000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 4;
                    p.Location = 4;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 1;
                    p.Name = "Cai gi day";
                    p.Picture = ConvertFileToProductImage(imgPath + "IC20V2_1.png");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 9;
                    p.Location = 1;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 2;
                    p.Name = "Loa mini";
                    p.Picture = ConvertFileToProductImage(imgPath + "Bo-loa-mini-hinh-trai-tao.png");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 2;
                    p.Location = 2;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 3;
                    p.Name = "Loa aonike-s582";
                    p.Picture = ConvertFileToProductImage(imgPath + "aonike-s582.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 2;
                    p.Location = 3;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 4;
                    p.Name = "iPhone 5";
                    p.Picture = ConvertFileToProductImage(imgPath + "650_img1.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 1;
                    p.Location = 4;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 1;
                    p.Name = "Xe gan may";
                    p.Picture = ConvertFileToProductImage(imgPath + "1365524302-IMG_0206.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 7;
                    p.Location = 1;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 2;
                    p.Name = "Bep ga";
                    p.Picture = ConvertFileToProductImage(imgPath + "Thumb_3432_jpg-03.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 3;
                    p.Location = 2;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 3;
                    p.Name = "quat-hop-kdk-sd30x";
                    p.Picture = ConvertFileToProductImage(imgPath + "quat-hop-kdk-sd30x-410846j18726.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 9;
                    p.Location = 3;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 4;
                    p.Name = "Laptop mau do";
                    p.Picture = ConvertFileToProductImage(imgPath 
                        + "e3148e2fb3acdb654334c58124a1ecb15fddc31e.jpeg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 6;
                    p.Location = 4;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 1;
                    p.Name = "tivi-lcd-lg";
                    p.Picture = ConvertFileToProductImage(imgPath 
                        + "tivi-lcd-lg-32ld330-32-inch-1366-x-768-pixel1_360.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 2;
                    p.Location = 1;
                    _db.Products.Add(p);

                    p = new Product();
                    p.UserId = 2;
                    p.Name = "May anh Leica_10504_M7";
                    p.Picture = ConvertFileToProductImage(imgPath 
                        + "Leica_10504_M7_TTL_72_Rangefinder_246982.jpg");
                    p.Description = "sdfjsdlkf";
                    p.Price = decimal.Parse("5000000");
                    p.ProductKey = "Dien thoai";
                    p.PostingDate = DateTime.Now;
                    p.Available = true;
                    p.CategoryId = 2;
                    p.Location = 2;
                    _db.Products.Add(p);

                    _db.SaveChanges();
                }

            }
            catch (Exception er)
            {
                Console.WriteLine(er.ToString());
            }

            return true;
        }

        private ProductDao()
        {
            _db = DatabaseContexts.Instance;

            //IniatilazeData();
        }

        public List<Category> GetCategories()
        {
            return _db.Categories.ToList();
        }

        public List<City> GetCities()
        {
            return _db.Cities.ToList();
        } 

        public List<Product> GetProducts()
        {
            //var ps = _db.Products.ToList();
            //foreach (var p in ps)
            //{
            //    p.User = AccountDao.Instance.GetUser(p.UserId);
            //}

            return _db.Products.ToList();
        }

        public Product GetProdcut(int productId)
        {
            return _db.Products.Find(productId);
        }

        public List<FollowProduct> GetFollowProducts()
        {
            return _db.FollowProducts.ToList();
        }

        public List<ProductImage> GetProductImages()
        {
            return _db.ProductImages.ToList();
        }

        public void AddProduct(string productName, int selectedCatId, int selectedPos, 
            string desc, string productPrice, int userId, string productKey)
        {
            Product product = new Product();
            product.Name = productName;
            product.CategoryId = selectedCatId;
            product.Location = selectedPos;
            product.Description = desc;
            product.Price = decimal.Parse(productPrice);
            product.PostingDate = DateTime.Now;
            product.UserId = userId;
            product.ProductKey = productKey;
            product.Picture = "Product-Value.jpg";

            _db.Products.Add(product);
            _db.SaveChanges();
        }

        public bool DeleteProduct(int productId)
        {
            var product = _db.Products.Find(productId);

            if (product != null)
            {
                var list = _db.ProductImages.Where(proImg => proImg.ProductId == product.Id).ToList();

                for (int i = 0; i < list.Count; i++)
                {
                    _db.ProductImages.Remove(list[i]);
                }

                _db.Products.Remove(product);
                _db.SaveChanges();

                return true;
            }

            return false;
        }

        public bool AddPicture(int productId, int index, string fileName)
        {
            var products = _db.Products.Where(p => p.Id == productId).ToList();

            if (products.Count > 0)
            {
                Product product = products[products.Count - 1];

                if (product != null)
                {
                    product.Picture = fileName;

                    if (index == 0)
                    {
                        product.Picture = fileName;
                    }
                    else
                    {
                        ProductImage p = new ProductImage();
                        p.ProductId = product.Id;
                        p.ImageLink = fileName;
                        _db.ProductImages.Add(p);
                    }

                    _db.SaveChanges();

                    return true;
                }
            }

            return false;
        }

        public bool EditProduct(int productId, string productName, int selectedCatId, int selectedPos, 
            string desc, string productPrice, int userId, string productKey)
        {
            var product = _db.Products.Find(productId);

            if (product != null)
            {
                product.Name = productName;
                product.CategoryId = selectedCatId;
                product.Location = selectedPos;
                product.Description = desc;
                product.Price = decimal.Parse(productPrice);
                product.UserId = userId;
                product.ProductKey = productKey;

                _db.SaveChanges();

                return true;
            }

            return false;
        }

        public bool EditPicture(int index, string fileName, Product product)
        {
            if (product != null)
            {
                if (index == 0 && product.Picture == "Product-Value.jpg")
                {
                    product.Picture = fileName;
                }

                else
                {
                    ProductImage p = new ProductImage();
                    p.ProductId = product.Id;
                    p.ImageLink = fileName;
                    _db.ProductImages.Add(p);
                }

                _db.SaveChanges();

                return true;
            }

            return false;
        }

        public int UploadMainPicture(int pictureId)
        {
            ProductImage p = _db.ProductImages.FirstOrDefault(pi => pi.Id == pictureId);
            Product pro = _db.Products.FirstOrDefault(pr => pr.Id == p.ProductId);

            if (pro != null && p != null)
            {
                string temp = pro.Picture;
                pro.Picture = p.ImageLink;
                p.ImageLink = temp;

                _db.SaveChanges();

                return pro.Id;
            }

            return -1;
        }

        public bool UploadPicture(int productId, string content)
        {
            var p = GetProdcut(productId);

            if (p != null)
            {
                p.Picture = content;
                _db.SaveChanges();
            }

            return false;
        }

        public int DeletePicture(int pictureId)
        {
            ProductImage p = _db.ProductImages.FirstOrDefault(pi => pi.Id == pictureId);

            if (p != null)
            {
                _db.ProductImages.Remove(p);
                _db.SaveChanges();

                Product pro = _db.Products.FirstOrDefault(pr => pr.Id == p.ProductId);
                if (pro != null)
                {
                    return pro.Id;
                }
            }

            return -1;
        }

        public int DeleteMainPicture(int productId)
        {
            Product pro = _db.Products.FirstOrDefault(pr => pr.Id == productId);

            if (pro != null)
            {
                pro.Picture = "Product-Value.jpg";
                _db.SaveChanges();

                return pro.Id;
            }

            return -1;
        }
    }
}
