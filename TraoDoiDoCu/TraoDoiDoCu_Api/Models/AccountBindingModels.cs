﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace TraoDoiDoCu_Api.Models
{
    // Models used as parameters to AccountController actions.

    public class AddExternalLoginBindingModel
    {
        [Required]
        [Display(Name = "External access token")]
        public string ExternalAccessToken { get; set; }
    }

    public class ChangePasswordBindingModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterBindingModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterExternalBindingModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class RemoveLoginBindingModel
    {
        [Required]
        [Display(Name = "Login provider")]
        public string LoginProvider { get; set; }

        [Required]
        [Display(Name = "Provider key")]
        public string ProviderKey { get; set; }
    }

    public class SetPasswordBindingModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    #region Login/Register

    public class LoginViewModel
    {
        [Required]
        [StringLength(25, MinimumLength = 5, ErrorMessage = "Tên tài khoản nhiều hơn 5 kí tự và ít hơn 25 kí tự.")]
        [Display(Name = "Tên tài khoản:")]
        public string UserName { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 6, ErrorMessage = "Mật khẩu nhiều hơn 5 kí tự và ít hơn 50 kí tự.")]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [Display(Name = "Nhớ mật khẩu")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [StringLength(25, MinimumLength = 5, ErrorMessage = "Tên tài khoản ít nhất 6 kí tự và nhiều nhất 25 kí tự.")]
        [Display(Name = "Tên tài khoản:")]
        public string UserName { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 6, ErrorMessage = "Mật khẩu ít nhất 6 kí tự và nhiều nhất 50 kí tự.")]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Xác nhận mật khẩu")]
        [Compare("Password", ErrorMessage = "Mật khẩu và mật khẩu xác nhận không trùng nhau.")]
        public string ConfirmPassword { get; set; }
        [Required]
        [EmailAddress]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [StringLength(50, MinimumLength = 2, ErrorMessage = "Họ nhiều hơn 1 kí tự và ít hơn 50 kí tự.")]
        [DataType(DataType.Password)]
        [Display(Name = "Họ")]
        public string LastName { get; set; }

        [StringLength(50, MinimumLength = 2, ErrorMessage = "Tên nhiều hơn 2 kí tự và ít hơn 50 kí tự.")]
        [DataType(DataType.Password)]
        [Display(Name = "Tên")]
        public string FirstName { get; set; }

        [StringLength(15, MinimumLength = 6, ErrorMessage = "Số điện thoại nhiều hơn 5 kí tự và ít hơn 16 kí tự.")]
        [DataType(DataType.Password)]
        [Display(Name = "Số điện thoại")]
        public string Phone { get; set; }
    }

    #endregion
}
