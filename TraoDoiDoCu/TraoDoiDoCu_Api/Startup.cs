﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(TraoDoiDoCu_Api.Startup))]

namespace TraoDoiDoCu_Api
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
