﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BusinessLogicLayer;
using DataTransferLayer.Accounts;
using DataTransferLayer.Accounts.Api;

namespace TraoDoiDoCu_Api.Controllers
{
    public class UserActionController : ApiController
    {
        #region Helper
        public HttpResponseMessage CreateResponse<T>(HttpStatusCode statusCode, T data)
        {
            return Request.CreateResponse(statusCode, data);
        }

        public HttpResponseMessage CreateResponse(HttpStatusCode statusCode)
        {
            return Request.CreateResponse(statusCode);
        }

        #endregion

        [HttpGet]
        [Route("api/userAction/comment/all")]
        public HttpResponseMessage GetComments()
        {
            List<Comment> mess = UserActionBlo.GetComments();

            if (mess == null)
            {
                return CreateResponse(HttpStatusCode.NoContent);
            }

            List<CommentApi> nl = new List<CommentApi>();
            foreach (var c in mess)
            {
                nl.Add(new CommentApi(c));
            }

            return CreateResponse(HttpStatusCode.OK, nl);
        }

        [HttpPost]
        [Route("api/userAction/comment/add/{content}")]
        public HttpResponseMessage AddComments(string content)
        {         
            if (string.IsNullOrEmpty(content))
            {
                return CreateResponse(HttpStatusCode.BadRequest);
            }

            try
            {
                string[] parts = content.Split(new[] { WebApiHelper.Seperator }, StringSplitOptions.None);
                if (parts.Length == 3)
                {
                    string userName = parts[0];
                    int productId = int.Parse(parts[1]);
                    string desc = parts[2];

                    if (UserActionBlo.AddComment(userName, productId, desc))
                    {
                        return CreateResponse(HttpStatusCode.OK);
                    }
                }
            }
            catch (Exception ex)
            {              
                return CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
            

            return CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route("api/userAction/message/all")]
        public HttpResponseMessage GetMessages()
        {
            List<Message> mess = UserActionBlo.GetMessages();

            if (mess == null)
            {
                return CreateResponse(HttpStatusCode.NoContent);
            }

            List<MessageApi> nl = new List<MessageApi>();
            foreach (var c in mess)
            {
                nl.Add(new MessageApi(c));
            }

            return CreateResponse(HttpStatusCode.OK, nl);
        }

        [HttpPost]
        [Route("api/userAction/follow/check/{content}")]
        public HttpResponseMessage CheckFollow(string content)
        {
            if (string.IsNullOrEmpty(content))
            {
                return CreateResponse(HttpStatusCode.BadRequest);
            }

            try
            {
                string[] parts = content.Split(new[] { WebApiHelper.Seperator }, StringSplitOptions.None);
                if (parts.Length == 2)
                {
                    string userName = parts[0];
                    int productId = int.Parse(parts[1]);

                    if (UserActionBlo.CheckFollow(userName, productId))
                    {
                        return CreateResponse(HttpStatusCode.OK);
                    }
                }
            }
            catch (Exception ex)
            {
                return CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
            }

            return CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route("api/userAction/follow/unfollow/{content}")]
        public HttpResponseMessage Unfollow(string content)
        {
            if (string.IsNullOrEmpty(content))
            {
                return CreateResponse(HttpStatusCode.BadRequest);
            }

            try
            {
                string[] parts = content.Split(new[] { WebApiHelper.Seperator }, StringSplitOptions.None);
                if (parts.Length == 2)
                {
                    string userName = parts[0];
                    int productId = int.Parse(parts[1]);

                    if (UserActionBlo.UnFollow(userName, productId))
                    {
                        return CreateResponse(HttpStatusCode.OK);
                    }
                }
            }
            catch (Exception ex)
            {
                return CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
            }

            return CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route("api/userAction/vote/all")]
        public HttpResponseMessage GetVoteLogs()
        {
            List<VoteLog> mess = UserActionBlo.GetVoteLogs();

            if (mess == null)
            {
                return CreateResponse(HttpStatusCode.NoContent);
            }

            List<VoteLogApi> nl = new List<VoteLogApi>();
            foreach (var c in mess)
            {
                nl.Add(new VoteLogApi(c));
            }

            return CreateResponse(HttpStatusCode.OK, nl);
        }

        [HttpPost]
        [Route("api/userAction/vote/isVoted/{content}")]
        public HttpResponseMessage IsVoted(string content)
        {
            if (string.IsNullOrEmpty(content))
            {
                return CreateResponse(HttpStatusCode.BadRequest);
            }

            try
            {
                string[] parts = content.Split(new[] {WebApiHelper.Seperator}, StringSplitOptions.None);
                if (parts.Length == 3)
                {
                    int sectionId = int.Parse(parts[0]);
                    string userName = parts[1];
                    int autoId = int.Parse(parts[2]);

                    if (UserActionBlo.IsVoted(sectionId, userName, autoId))
                    {
                        return CreateResponse(HttpStatusCode.OK);
                    }
                }
            }
            catch (Exception ex)
            {
                return CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
            }

            return CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route("api/userAction/rating/send/{content}")]
        public HttpResponseMessage SendRating(string content)
        {
            if (string.IsNullOrEmpty(content))
            {
                return CreateResponse(HttpStatusCode.BadRequest);
            }

            try
            {
                string[] parts = content.Split(new[] { WebApiHelper.Seperator }, StringSplitOptions.None);
                if (parts.Length == 4)
                {
                    int sectionId = int.Parse(parts[0]);
                    string userName = parts[1];
                    int autoId = int.Parse(parts[2]);
                    int thisVote = int.Parse(parts[3]);

                    if (UserActionBlo.SendRating(sectionId, userName, autoId, thisVote))
                    {
                        return CreateResponse(HttpStatusCode.OK);
                    }
                }
            }
            catch (Exception ex)
            {              
                return CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
            }            

            return CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route("api/userAction/report/reportFor/{content}")]
        public HttpResponseMessage Report(string content)
        {
            if (string.IsNullOrEmpty(content))
            {
                return CreateResponse(HttpStatusCode.BadRequest);
            }

            try
            {
                string[] parts = content.Split(new[] { WebApiHelper.Seperator }, StringSplitOptions.None);
                if (parts.Length == 6)
                {
                    string userName = parts[0];
                    int reportForId = int.Parse(parts[1]);
                    int productId = int.Parse(parts[2]);
                    string reasonP = parts[3];
                    string reasonU = parts[4];
                    string desc = parts[5];

                    if (UserActionBlo.ReportPost(userName, reportForId, productId, reasonP, reasonU, desc))
                    {
                        return CreateResponse(HttpStatusCode.OK);
                    }
                }
            }
            catch (Exception ex)
            {
                return CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
            }

            return CreateResponse(HttpStatusCode.BadRequest);
        }
    }
}
