﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BusinessLogicLayer;
using DataTransferLayer.Accounts;
using DataTransferLayer.Accounts.Api;

namespace TraoDoiDoCu_Api.Controllers
{
    public class DashboardController : ApiController
    {
        #region Helper
        public HttpResponseMessage CreateResponse<T>(HttpStatusCode statusCode, T data)
        {
            return Request.CreateResponse(statusCode, data);
        }

        public HttpResponseMessage CreateResponse(HttpStatusCode statusCode)
        {
            return Request.CreateResponse(statusCode);
        }

        #endregion

        [HttpPut]
        [Route("api/dashboard/add/{content}")]
        public HttpResponseMessage AddNotification(string content)
        {
            GlobalDataBlo.AddNotification(content);
            return CreateResponse(HttpStatusCode.OK);
        }

        [HttpPut]
        [Route("api/dashboard/notify/{content}")]
        public HttpResponseMessage Notify(string content)
        {
            string[] parts = content.Split(new[] {"!@#$%?|"}, StringSplitOptions.None);
            if (parts.Length != 2)
            {
                return CreateResponse(HttpStatusCode.BadRequest);
            }

            string noiDungThongBao = parts[0];
            string nguoiNhan = parts[1];

            if (nguoiNhan != null)
            {
                // Code gui tin nhan
            }
            else
            {
                GlobalDataBlo.AddNotification(noiDungThongBao);
            }

            return CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        [Route("api/dashboard/blockUser/{listUser}")]
        public HttpResponseMessage BlockUser(string listUser)
        {
            string[] users = listUser.Split(',');
            UserActionBlo.BanAccount(users.ToList());

            return CreateResponse(HttpStatusCode.OK);
        }

        [HttpDelete]
        [Route("api/dashboard/deleteUser/{listUser}")]
        public HttpResponseMessage DeleteUser(string listUser)
        {
            string[] users = listUser.Split(',');
            AccountBlo.DeleteUsers(users.ToList());

            return CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        [Route("api/dashboard/checkBanAccount")]
        public HttpResponseMessage CheckBanAccount()
        {
            UserActionBlo.CheckBanAccount();

            return CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [Route("api/dashboard/search/{term}")]
        public HttpResponseMessage Search(string term)
        {
            return CreateResponse(HttpStatusCode.OK, AccountBlo.SearchUsernames(term));
        }

        [HttpGet]
        [Route("api/dashboard/searchUser/{userName}")]
        public HttpResponseMessage SearchUser(string userName)
        {
            return CreateResponse(HttpStatusCode.OK, AccountBlo.SearchUsernames(userName));
        }
    }
}
