﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Hosting;
using System.Web.Http;
using BusinessLogicLayer;
using DataTransferLayer.Accounts;
using DataTransferLayer.Accounts.Api;
using DataTransferLayer.Products;
using DataTransferLayer.Products.Api;

namespace TraoDoiDoCu_Api.Controllers
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ProductController : ApiController
    {
        /// <summary>
        /// Function:
        ///     - Get all categories
        ///     - Get all ciies
        ///     - Get all products
        ///     - Get product by id
        ///     - Get follow products bu user's id
        ///     - Get images by product's id
        ///     - Get products by category's id
        ///     - Get products by name
        ///     - Get products by city's id
        /// 
        ///     - Add product
        ///     - Delete product
        ///     - Add product's picture
        ///     - Delete product's picture
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="statusCode"></param>
        /// <param name="data"></param>
        /// <returns></returns>

        #region Helper
        public HttpResponseMessage CreateResponse<T>(HttpStatusCode statusCode, T data)
        {
            return Request.CreateResponse(statusCode, data);
        }

        public HttpResponseMessage CreateResponse(HttpStatusCode statusCode)
        {
            return Request.CreateResponse(statusCode);
        }

        #endregion

        [HttpGet]
        [Route("api/product/initialize")]
        public HttpResponseMessage InitializeDatabase()
        {
            if (ProductBlo.InilializeDatabase(HostingEnvironment.MapPath("~/App_Data/ProductImage/")))
            {
                return CreateResponse(HttpStatusCode.OK);
            }

            return CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route("api/product/categories")]
        public HttpResponseMessage GetCategories_API()
        {
            List<Category> categories = ProductBlo.GetCategories();
            if (categories == null)
            {
                return CreateResponse(HttpStatusCode.NoContent);
            }

            List<CategoryApi> nl = new List<CategoryApi>();
            foreach (var c in categories)
            {
                nl.Add(new CategoryApi(c));
            }

            return CreateResponse(HttpStatusCode.OK, nl);
        }

        [HttpGet]
        [Route("api/product/cities")]
        public HttpResponseMessage GetCities_API()
        {
            List<City> cities = ProductBlo.GetCities();
            if (cities == null)
            {
                return CreateResponse(HttpStatusCode.NoContent);
            }

            List<CityApi> nl = new List<CityApi>();
            foreach (var c in cities)
            {
                nl.Add(new CityApi(c));
            }

            return CreateResponse(HttpStatusCode.OK, nl);
        }

        public static byte[] ReadImageFile(string imageLocation)
        {
            byte[] imageData;
            FileInfo fileInfo = new FileInfo(imageLocation);
            long imageFileLength = fileInfo.Length;
            FileStream fs = new FileStream(imageLocation, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            imageData = br.ReadBytes((int)imageFileLength);
            return imageData;
        }

        [HttpGet]
        [Route("api/product/all")]
        public HttpResponseMessage GetProducts_API()
        {
            List < Product > lst = ProductBlo.GetProducts();
            if (lst == null)
            {
                return CreateResponse(HttpStatusCode.NoContent);
            }

            List<ProductApi> nl = new List<ProductApi>();
            foreach (var p in lst)
            {
                ProductApi np = new ProductApi(p);

                //try
                //{
                //    string imgPath = HostingEnvironment.MapPath("~/App_Data/ProductImage/") + p.Picture;
                //    var bytes = ReadImageFile(imgPath);
                //    var base64 = Convert.ToBase64String(bytes);
                //    np.Picture = string.Format("data:{0};base64,{1}", "image/png", base64);

                //}
                //catch (Exception ex)
                //{
                //    Console.WriteLine(ex.ToString());
                //}          

                nl.Add(np);
            }

            return CreateResponse(HttpStatusCode.OK, nl);
        }

        [HttpGet]
        [Route("api/product/{id:int}")]
        public HttpResponseMessage GetProductById_API(int id)
        {
            Product product = ProductBlo.GetProduct(id);

            if (product == null)
            {
                return CreateResponse(HttpStatusCode.NoContent);
            }

            ProductApi n = new ProductApi(product);

            return CreateResponse(HttpStatusCode.OK, n);
        }

        [HttpGet]
        [Route("api/product/getLast/{userId}")]
        public HttpResponseMessage GetLastProductBy(int userId)
        {
            Product product = ProductBlo.GetLastProductBy(userId);

            if (product == null)
            {
                return CreateResponse(HttpStatusCode.NoContent);
            }

            ProductApi n = new ProductApi(product);

            return CreateResponse(HttpStatusCode.OK, n);
        }


        [HttpGet]
        [Route("api/product/productImage/all")]
        public HttpResponseMessage GetProductImages()
        {
            List<ProductImage> fps = ProductBlo.GetProductImages();

            if (fps == null)
            {
                return CreateResponse(HttpStatusCode.NoContent);
            }

            List<ProductImageApi> nl = new List<ProductImageApi>();
            foreach (var c in fps)
            {
                nl.Add(new ProductImageApi(c));
            }

            return CreateResponse(HttpStatusCode.OK, nl);
        }

        [HttpPut]
        [Route("api/product/productImage/add/{id:int}")]
        public HttpResponseMessage UploadProductPicture(int id, string content)
        {
            try
            {
                if (ProductBlo.UploadPicture(id, content))
                {
                    return CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }               

            return CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPut]
        [Route("api/product/productImage/replaceMain/{id}")]
        public HttpResponseMessage ReplaceMainPicture(int id)
        {
            if (ProductBlo.UploadMainPicture(id) > 0)
            {
                return CreateResponse(HttpStatusCode.OK);
            }

            return CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPut]
        [Route("api/product/productImage/deleteMain/{id}")]
        public HttpResponseMessage DeleteMainPicture(int id)
        {
            if (ProductBlo.DeleteMainPicture(id) > 0)
            {
                return CreateResponse(HttpStatusCode.OK);
            }

            return CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route("api/product/followProduct/all")]
        public HttpResponseMessage GetFollowProducts()
        {
            List<FollowProduct> fps = ProductBlo.GetFollowProducts();
            if (fps == null)
            {
                return CreateResponse(HttpStatusCode.NoContent);
            }

            List<FollowProductApi> nl = new List<FollowProductApi>();
            foreach (var c in fps)
            {
                nl.Add(new FollowProductApi(c));
            }

            return CreateResponse(HttpStatusCode.OK, nl);
        }

        [HttpGet]
        [Route("api/product/follow/{id:int}")]
        public HttpResponseMessage GetFollowProductsByUserId_API(int id)
        {
            List<FollowProduct> allFollowProducts = ProductBlo.GetFollowProducts();

            List<FollowProduct> userFollowProducts = new List<FollowProduct>();

            List<Product> products = new List<Product>();

            foreach (FollowProduct followProduct in allFollowProducts)
            {
                if (followProduct.UserId == id)
                {
                    userFollowProducts.Add(followProduct);
                }
            }

            if (userFollowProducts.Count == 0)
            {
                return CreateResponse(HttpStatusCode.NoContent);
            }

            foreach (FollowProduct userFollowProduct in userFollowProducts)
            {
                products.Add(ProductBlo.GetProduct(userFollowProduct.ProductId));
            }

            List<ProductApi> nl = new List<ProductApi>();
            foreach (var p in products)
            {
                nl.Add(new ProductApi(p));
            }

            return CreateResponse(HttpStatusCode.OK, nl);
        }

        [HttpGet]
        [Route("api/product/image/{id:int}")]
        public HttpResponseMessage GetImageByProductId_API(int id)
        {
            List<ProductImage> lst = ProductBlo.GetProductImages();

            List<ProductImage> productImages = new List<ProductImage>();

            foreach (ProductImage productImage in lst)
            {
                if (productImage.ProductId == id)
                {
                    productImages.Add(productImage);
                }
            }

            if (productImages.Count == 0)
            {
                return CreateResponse(HttpStatusCode.NoContent);
            }

            List<ProductImageApi> nl = new List<ProductImageApi>();
            foreach (var p in productImages)
            {
                nl.Add(new ProductImageApi(p));
            }

            return CreateResponse(HttpStatusCode.OK, nl);
        }

        [HttpGet]
        [Route("api/product/category/{categoryId}")]
        public HttpResponseMessage GetProductByCategoryId_API(int categoryId)
        {
            List<Product> lst = ProductBlo.GetProducts();
            List<Product> products = new List<Product>();

            foreach (Product product in lst)
            {
                if (product.CategoryId == categoryId)
                {
                    products.Add(product);
                }
            }

            if (products.Count == 0)
            {
                return CreateResponse(HttpStatusCode.NoContent);
            }

            List<ProductApi> nl = new List<ProductApi>();
            foreach (var p in products)
            {
                nl.Add(new ProductApi(p));
            }

            return CreateResponse(HttpStatusCode.OK, nl);
        }

        [HttpGet]
        [Route("api/product/category/name/{categoryName}")]
        public HttpResponseMessage GetProductByCategoryName_API(string categoryName)
        {
            List<Category> cats = ProductBlo.GetCategories(categoryName);
            if (cats.Count < 1)
            {
                return CreateResponse(HttpStatusCode.BadRequest);
            }

            List<Product> lst = ProductBlo.GetProducts();
            List<Product> products = new List<Product>();

            foreach (Product product in lst)
            {
                var exists = cats.Where(x => x.Id == product.CategoryId).ToList();
                if (exists.Count > 0)
                {
                    products.Add(product);
                }
            }

            if (products.Count == 0)
            {
                return CreateResponse(HttpStatusCode.NoContent);
            }

            List<ProductApi> nl = new List<ProductApi>();
            foreach (var p in products)
            {
                nl.Add(new ProductApi(p));
            }

            return CreateResponse(HttpStatusCode.OK, nl);
        }

        [HttpGet]
        [Route("api/product/{name}")]
        public HttpResponseMessage GetProductByName_API(string name)
       {
            List<Product> lst = ProductBlo.GetProducts();
            List<Product> products = new List<Product>();

            foreach (Product product in lst)
            {
                if (product.Name.Contains(name))
                {
                    products.Add(product);
                }
            }

            if (products.Count == 0)
            {
                return CreateResponse(HttpStatusCode.NoContent);
            }

            List<ProductApi> nl = new List<ProductApi>();
            foreach (var p in products)
            {
                nl.Add(new ProductApi(p));
            }

            return CreateResponse(HttpStatusCode.OK, nl);
        }


        [HttpGet]
        [Route("api/product/city/{id:int}")]
        public HttpResponseMessage GetProductByCities_API(int id)
        {
            List<Product> lst = ProductBlo.GetProducts();
            List<Product> products = new List<Product>();

            foreach (Product product in lst)
            {
                if (product.City.Id == id)
                {
                    products.Add(product);
                }
            }

            List<ProductApi> nl = new List<ProductApi>();
            foreach (var p in products)
            {
                nl.Add(new ProductApi(p));
            }

            return CreateResponse(HttpStatusCode.OK, nl);
        }

        //[HttpPut]
        //[Route("api/product/add")]
        //public HttpResponseMessage AddProduct([FromBody] ProductApi product)
        //{
            
        //    ProductBlo.AddProduct(product.Name, 
        //                              product.CategoryId, 
        //                              product.Location, 
        //                              product.Description,
        //                              product.Price.ToString(CultureInfo.InvariantCulture),
        //                              product.UserId,
        //                              product.ProductKey);
        //    return CreateResponse(HttpStatusCode.OK);
        //}

        [HttpPut]
        [Route("api/product/add/{content}")]
        public HttpResponseMessage AddProduct(string content)
        {
            try
            {
                string[] parts = content.Split(new[] { WebApiHelper.Seperator }, StringSplitOptions.None);
                if (parts.Length == 7)
                {
                    ProductBlo.AddProduct(parts[0], int.Parse(parts[1]), int.Parse(parts[2]),
                        parts[3], parts[4], int.Parse(parts[5]), parts[6]);

                    return CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            
            return CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpDelete]
        [Route("api/product/delete/{id:int}")]
        public HttpResponseMessage DeleteProduct_API(int id)
        {
            ProductBlo.DeleteProduct(id);
            return CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        [Route("api/product/addimage")]
        public HttpResponseMessage AddProductImage([FromBody] ProductImageApi productImage)
        {
            ProductBlo.AddPicture(productImage.ProductId, 0, productImage.ImageLink);
            return CreateResponse(HttpStatusCode.OK);
        }

        [HttpDelete]
        [Route("api/product/deleteimage/{id:int}")]
        public HttpResponseMessage DeleteProductImage(int id)
        {
            ProductBlo.DeletePicture(id);
            return CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        [Route("api/product/edit/{content}")]
        public HttpResponseMessage EditProduct(string content)
        {
            try
            {
                string[] parts = content.Split(new[] { WebApiHelper.Seperator }, StringSplitOptions.None);
                if (parts.Length == 8)
                {
                    ProductBlo.EditProduct(int.Parse(parts[0]), parts[1], int.Parse(parts[2]), int.Parse(parts[3]),
                        parts[4], parts[5], int.Parse(parts[6]), parts[7]);

                    return CreateResponse(HttpStatusCode.OK);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }           

            return CreateResponse(HttpStatusCode.BadRequest);
        }
    }
}
