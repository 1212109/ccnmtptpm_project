﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BusinessLogicLayer;
using DataTransferLayer.Accounts;
using DataTransferLayer.Accounts.Api;

namespace TraoDoiDoCu_Api.Controllers
{
    public class AccountManagerController : ApiController
    {
        #region Helper
        public HttpResponseMessage CreateResponse<T>(HttpStatusCode statusCode, T data)
        {
            return Request.CreateResponse(statusCode, data);
        }

        public HttpResponseMessage CreateResponse(HttpStatusCode statusCode)
        {
            return Request.CreateResponse(statusCode);
        }

        #endregion


        [HttpGet]
        [Route("api/accountManager/account/all")]
        public HttpResponseMessage GetAccounts()
        {
            List<User> categories = AccountBlo.GetUsers();

            if (categories == null)
            {
                return CreateResponse(HttpStatusCode.NoContent);
            }

            List<UserApi> nl = new List<UserApi>();
            foreach (var c in categories)
            {
                nl.Add(new UserApi(c));
            }

            return CreateResponse(HttpStatusCode.OK, nl);
        }

        [HttpGet]
        [Route("api/accountManager/account/{id:int}")]
        public HttpResponseMessage GetAccount(int id)
        {
            User user = AccountBlo.GetUser(id);

            if (user == null)
            {
                return CreateResponse(HttpStatusCode.NoContent);
            }

            var n = new UserApi(user);

            return CreateResponse(HttpStatusCode.OK, n);
        }

        [HttpGet]
        [Route("api/accountManager/account/search/{userName}")]
        public HttpResponseMessage GetAccount(string userName)
        {
            List<User> us = AccountBlo.GetUsers();
            var exists = us.Where(x => x.UserName == userName).ToList();
            var uss = new List<UserApi>();

            if (exists.Count > 0)
            {
                foreach (var x in exists)
                {
                    var u = new UserApi(x);
                    uss.Add(u);
                }
            }
            else
            {
                return CreateResponse(HttpStatusCode.NoContent);
            }

            return CreateResponse(HttpStatusCode.OK, uss);
        }

        [HttpPost]
        [Route("api/accountManager/account/isValidLogin/{content}")]
        public HttpResponseMessage IsValidLogin(string content)
        {
            string[] parts = content.Split(new[] {WebApiHelper.Seperator}, StringSplitOptions.None);
            if (parts.Length == 2)
            {
                string userName = parts[0];
                string password = parts[1];

                if (AccountBlo.LoginIsValid(userName, password))
                {
                    return CreateResponse(HttpStatusCode.OK);
                }
            }           

            return CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route("api/accountManager/account/isActived/{userName}")]
        public HttpResponseMessage IsActivedAccount(string userName)
        {
            if (AccountBlo.IsActivedAccount(userName))
            {
                return CreateResponse(HttpStatusCode.OK);
            }

            return CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route("api/accountManager/account/exists/{userName}")]
        public HttpResponseMessage CheckExistenceAccount(string userName, string email)
        {
            try
            {
                if (AccountBlo.CheckExistenceAccount(email, userName))
                {
                    return CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            

            return CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route("api/accountManager/email/exists/{content}")]
        public HttpResponseMessage CheckExistecenEmail(string content, string email)
        {
            if (AccountBlo.CheckMailExistence(email))
            {
                return CreateResponse(HttpStatusCode.OK);
            }

            return CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route("api/accountManager/account/register/{content}")]
        public HttpResponseMessage RegisterAccount(string content, string email)
        {
            try
            {
                string[] parts = content.Split(new[] {WebApiHelper.Seperator}, StringSplitOptions.None);
                if (parts.Length == 6)
                {
                    User u = new User();
                    u.UserName = parts[0];
                    u.Password = parts[1];
                    u.Email = email;
                    u.LastName = parts[3];
                    u.FirstName = parts[4];
                    u.Phone = parts[5];

                    if (AccountBlo.RegisterAccount(u))
                    {
                        return CreateResponse(HttpStatusCode.OK, content);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return CreateResponse(HttpStatusCode.BadRequest, content);
        }

        [HttpPost]
        [Route("api/accountManager/account/active/{userName}")]
        public HttpResponseMessage ActivateAccount(string userName, string activationCode)
        {
            try
            {
                if (AccountBlo.ActiveAccount(userName, activationCode))
                {
                    return CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }          

            return CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route("api/accountManager/account/setUpResetPassword/{content}")]
        public HttpResponseMessage SetUpResetPassword(string content, string email)
        {
            if (AccountBlo.SetUpResetPassword(email))
            {
                return CreateResponse(HttpStatusCode.OK);
            }

            return CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route("api/accountManager/account/checkResetPassword/{resetCode}")]
        public HttpResponseMessage CheckResetPassword(string resetCode, string email)
        {
            try
            {
                if (AccountBlo.CheckResetPassword(email, resetCode))
                {
                    return CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            
            return CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route("api/accountManager/account/changePassword/{password}")]
        public HttpResponseMessage ChangePassword(string password, string resetCode, string email)
        {
            try
            {
                ResetPasswordDto a = new ResetPasswordDto();
                a.Email = email;
                a.Password = password;
                a.ResetCode = resetCode;

                if (AccountBlo.ChangePassword(a))
                {
                    return CreateResponse(HttpStatusCode.OK, password + resetCode + email);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }            

            return CreateResponse(HttpStatusCode.BadRequest, password + resetCode + email);
        }

        [HttpDelete]
        [Route("api/accountManager/account/delete/{id:int}")]
        public HttpResponseMessage DeleteAccount(int id)
        {
            if (AccountBlo.DeleteUsers(id))
            {
                return CreateResponse(HttpStatusCode.OK);
            }

            return CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route("api/accountManager/account/checkInvalidPassword/{content}")]
        public HttpResponseMessage CheckInvalidPassword(string content)
        {
            try
            {
                string[] parts = content.Split(new[] { WebApiHelper.Seperator }, StringSplitOptions.None);
                if (parts.Length == 2)
                {
                    if (AccountBlo.CheckInvalidPassword(int.Parse(parts[0]), parts[1]))
                    {
                        return CreateResponse(HttpStatusCode.OK);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }          

            return CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route("api/accountManager/account/update/{content}")]
        public HttpResponseMessage UpdateUserInfo(string content, string email)
        {
            try
            {
                string[] parts = content.Split(new[] { WebApiHelper.Seperator }, StringSplitOptions.None);
                if (parts.Length == 6)
                {
                    User updatedUserInfo = new User();
                    updatedUserInfo.Id = int.Parse(parts[0]);
                    updatedUserInfo.Password = parts[1];
                    updatedUserInfo.Email = email;
                    updatedUserInfo.LastName = parts[3];
                    updatedUserInfo.FirstName = parts[4];    
                    updatedUserInfo.Phone = parts[5];                   

                    if (AccountBlo.UpdateUserInfo(updatedUserInfo.Id, updatedUserInfo))
                    {
                        return CreateResponse(HttpStatusCode.OK);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }         

            return CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route("api/accountManager/account/getIdProductsFollowed/{userName}")]
        public HttpResponseMessage GetIdProductsFollowed(string userName)
        {
            List<int> rs = AccountBlo.GetIdProductsFollowed(userName);

            return CreateResponse(HttpStatusCode.OK, rs);
        }

        [HttpGet]
        [Route("api/accountManager/account/getProductsFollowed/{userName}")]
        public HttpResponseMessage GetProductsFollowed(string userName)
        {
            List<int> rs = AccountBlo.GetIdProductsFollowed(userName);
            var lstOfFollowedProduct = AccountBlo.GetProductsFollowed(rs);

            return CreateResponse(HttpStatusCode.OK, lstOfFollowedProduct);
        }

        [HttpPost]
        [Route("api/accountManager/account/deleteAFollowedProduct/{content}")]
        public HttpResponseMessage DeleteAFollowedProduct(string content)
        {
            try
            {
                string[] parts = content.Split(new[] { WebApiHelper.Seperator }, StringSplitOptions.None);
                if (parts.Length == 2)
                {
                    if (AccountBlo.DeleteAFollowedProduct(int.Parse(parts[0]), int.Parse(parts[1])))
                    {
                        return CreateResponse(HttpStatusCode.OK);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
                       
            return CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route("api/accountManager/account/uploadAccountImage/{content}")]
        public HttpResponseMessage UploadAccountImage(string content)
        {
            try
            {
                string[] parts = content.Split(new[] { WebApiHelper.Seperator }, StringSplitOptions.None);
                if (parts.Length == 2)
                {
                    if (AccountBlo.UploadAccountImage(int.Parse(parts[0]), parts[1]))
                    {
                        return CreateResponse(HttpStatusCode.OK);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            
            return CreateResponse(HttpStatusCode.OK);
        }
    }
}
