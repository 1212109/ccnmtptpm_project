﻿using System.ComponentModel.DataAnnotations;

namespace DataTransferLayer
{
    public class GlobalData
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Content { get; set; }
    }
}
