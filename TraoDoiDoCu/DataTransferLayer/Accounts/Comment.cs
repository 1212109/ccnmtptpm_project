﻿using System;
using System.ComponentModel.DataAnnotations;
using DataTransferLayer.Accounts.Api;
using DataTransferLayer.Products;

namespace DataTransferLayer.Accounts
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }

        public int ProductId { get; set; }
        //[ForeignKey("ProductId")]
        public Product Product { get; set; }

        public int UserId { get; set; }
        //[ForeignKey("UserId")]
        public User User { get; set; }

        public string TextContent { get; set; }

        public DateTime PostingDate { get; set; }

        public Comment()
        {
            
        }

        public Comment(CommentApi c)
        {
            if (c != null)
            {
                Id = c.Id;
                ProductId = c.ProductId;
                UserId = c.UserId;
                TextContent = c.TextContent;
                PostingDate = c.PostingDate;
            }         
        }
    }
}
