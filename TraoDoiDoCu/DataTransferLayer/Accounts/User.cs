﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataTransferLayer.Accounts.Api;
using DataTransferLayer.Products;

namespace DataTransferLayer.Accounts
{
    public class User
    {
        public User()
        {
            Comments = new HashSet<Comment>();
            FollowProducts = new HashSet<FollowProduct>();
            SentMessages = new HashSet<Message>();
            ReceivedMessages = new HashSet<Message>();
            Products = new HashSet<Product>();
            Reports = new HashSet<Report>();
            Reports1 = new HashSet<Report>();
            VoteLogs = new HashSet<VoteLog>();
        }

        public User(UserApi u)
        {
            if (u != null)
            {
                Id = u.Id;
                UserName = u.UserName;
                Password = u.Password;
                FirstName = u.FirstName;
                LastName = u.LastName;
                Email = u.Email;
                Admin = u.Admin;
                Ban = u.Ban;
                Rating = u.Rating;
                Phone = u.Phone;
                ReportId = u.ReportId;
                ActiveCode = u.ActiveCode;
                ResetPassword = u.ResetPassword;
                DateRequest = u.DateRequest;
                BanDate = u.BanDate;
                BanTime = u.BanTime;
                UserImage = u.UserImage;
            }           

            Comments = new HashSet<Comment>();
            FollowProducts = new HashSet<FollowProduct>();
            SentMessages = new HashSet<Message>();
            ReceivedMessages = new HashSet<Message>();
            Products = new HashSet<Product>();
            Reports = new HashSet<Report>();
            Reports1 = new HashSet<Report>();
            VoteLogs = new HashSet<VoteLog>();
        }

        [Key]
        public int Id { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public bool? Admin { get; set; }

        public bool? Ban { get; set; }

        public double? Rating { get; set; }

        public string Phone { get; set; }

        public int? ReportId { get; set; }

        public string ActiveCode { get; set; }

        public string ResetPassword { get; set; }

        public DateTime? DateRequest { get; set; }

        public DateTime? BanDate { get; set; }

        public int? BanTime { get; set; }

        public string UserImage { get; set; }

        public HashSet<Comment> Comments { get; set; }
        public HashSet<FollowProduct> FollowProducts { get; set; }
        public HashSet<Message> SentMessages { get; set; }
        public HashSet<Message> ReceivedMessages { get; set; }
        public HashSet<Product> Products { get; set; }
        public HashSet<Report> Reports { get; set; }
        public HashSet<Report> Reports1 { get; set; }
        public HashSet<VoteLog> VoteLogs { get; set; }
    }
}
