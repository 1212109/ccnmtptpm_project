﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataTransferLayer.Accounts.Api;

namespace DataTransferLayer.Accounts
{
    public class VoteLog
    {
        [Key]
        public int Id { get; set; }

        public int SectionId { get; set; } 

        public int? VoteForId { get; set; }
        //[ForeignKey("VoteForId")]
        public User User { get; set; }

        public string UserName { get; set; }

        public int Vote { get; set; }

        public bool Active { get; set; }

        public VoteLog()
        {
            
        }

        public VoteLog(VoteLogApi v)
        {
            if (v != null)
            {
                Id = v.Id;
                SectionId = v.SectionId;
                VoteForId = v.VoteForId;
                UserName = v.UserName;
                Vote = v.Vote;
                Active = v.Active;
            }           
        }
    }
}
