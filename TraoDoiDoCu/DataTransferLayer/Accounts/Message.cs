﻿using System;
using System.ComponentModel.DataAnnotations;
using DataTransferLayer.Accounts.Api;

namespace DataTransferLayer.Accounts
{
    public class Message
    {
        [Key]
        public int Id { get; set; }

        public int SenderId { get; set; }
        //[ForeignKey("SenderId")]
        //[InverseProperty("SentMessages")]
        public User Sender { get; set; }

        public int ReceiverId { get; set; }
        //[ForeignKey("ReceiverId")]
        //[InverseProperty("ReceivedMessages")]
        public User Receiver { get; set; }

        public string Content { get; set; }

        public DateTime DatePost { get; set; }

        public bool IsRead { get; set; }

        public Message()
        {
            
        }

        public Message(MessageApi m)
        {
            if (m != null)
            {
                Id = m.Id;
                SenderId = m.SenderId;
                ReceiverId = m.ReceiverId;
                Content = m.Content;
                DatePost = m.DatePost;
                IsRead = m.IsRead;
            }       
        }
    }
}
