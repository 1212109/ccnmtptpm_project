﻿using System.ComponentModel.DataAnnotations;
using DataTransferLayer.Products;

namespace DataTransferLayer.Accounts
{
    public class Report
    {
        [Key]
        public int Id { get; set; }

        public int UserId { get; set; }
        //[ForeignKey("UserId")]
        public virtual User User { get; set; }

        public int ReportForId { get; set; }
        //[ForeignKey("ReportForId")]
        public virtual User UserFor { get; set; }

        public int ProductId { get; set; }
        //[ForeignKey("ProductId")]
        public Product Product { get; set; }

        public string ReasonByProduct { get; set; }

        public string ReasonByUser { get; set; }

        public string Description { get; set; }   
    }
}
