﻿using System.ComponentModel.DataAnnotations;
using DataTransferLayer.Accounts.Api;
using DataTransferLayer.Products;

namespace DataTransferLayer.Accounts
{
    public class FollowProduct
    {
        [Key]
        public int Id { get; set; }

        public int UserId { get; set; }
        //[ForeignKey("UserId")]
        public User User { get; set; }

        public int ProductId { get; set; }
        //[ForeignKey("ProductId")]
        public Product Product { get; set; }

        public bool Active { get; set; }

        public FollowProduct()
        {
            
        }

        public FollowProduct(FollowProductApi f)
        {
            if (f != null)
            {
                Id = f.Id;
                UserId = f.UserId;
                ProductId = f.ProductId;
                Active = f.Active;
            }           
        }
    }
}
