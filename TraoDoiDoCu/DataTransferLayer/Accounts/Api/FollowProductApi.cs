﻿namespace DataTransferLayer.Accounts.Api
{
    public class FollowProductApi
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ProductId { get; set; }
        public bool Active { get; set; }

        public FollowProductApi()
        {
            
        }

        public FollowProductApi(FollowProduct f)
        {
            Id = f.Id;
            UserId = f.UserId;
            ProductId = f.ProductId;
            Active = f.Active;
        }
    }
}
