﻿namespace DataTransferLayer.Accounts.Api
{
    public class ReportApi
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ReportForId { get; set; }
        public int ProductId { get; set; }
        public string ReasonByProduct { get; set; }
        public string ReasonByUser { get; set; }
        public string Description { get; set; }

        public ReportApi()
        {
            
        }

        public ReportApi(Report r)
        {
            if (r != null)
            {
                Id = r.Id;
                UserId = r.UserId;
                ReportForId = r.ReportForId;
                ProductId = r.ProductId;
                ReasonByProduct = r.ReasonByProduct;
                ReasonByUser = r.ReasonByUser;
                Description = r.Description;
            }
        }
    }
}
