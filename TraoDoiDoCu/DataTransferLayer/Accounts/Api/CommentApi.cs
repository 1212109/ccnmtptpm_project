﻿using System;

namespace DataTransferLayer.Accounts.Api
{
    public class CommentApi
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int UserId { get; set; }
        public string TextContent { get; set; }
        public DateTime PostingDate { get; set; }

        public CommentApi()
        {
            
        }

        public CommentApi(Comment c)
        {
            if (c != null)
            {
                Id = c.Id;
                ProductId = c.ProductId;
                UserId = c.UserId;
                TextContent = c.TextContent;
                PostingDate = c.PostingDate;
            }          
        }
    }
}
