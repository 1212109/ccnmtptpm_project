﻿using System;

namespace DataTransferLayer.Accounts.Api
{
    public class MessageApi
    {
        public int Id { get; set; }
        public int SenderId { get; set; }
        public int ReceiverId { get; set; }
        public string Content { get; set; }
        public DateTime DatePost { get; set; }
        public bool IsRead { get; set; }

        public MessageApi()
        {
            
        }

        public MessageApi(Message m)
        {
            if (m != null)
            {
                Id = m.Id;
                SenderId = m.SenderId;
                ReceiverId = m.ReceiverId;
                Content = m.Content;
                DatePost = m.DatePost;
                IsRead = m.IsRead;
            }          
        }
    }
}
