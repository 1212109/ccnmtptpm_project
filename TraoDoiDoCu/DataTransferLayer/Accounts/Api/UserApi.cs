﻿using System;
using System.Collections.Generic;

namespace DataTransferLayer.Accounts.Api
{
    public class UserApi
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool? Admin { get; set; }
        public bool? Ban { get; set; }
        public double? Rating { get; set; }
        public string Phone { get; set; }
        public int? ReportId { get; set; }
        public string ActiveCode { get; set; }
        public string ResetPassword { get; set; }
        public DateTime? DateRequest { get; set; }
        public DateTime? BanDate { get; set; }
        public int? BanTime { get; set; }
        public string UserImage { get; set; }
        public List<int> Comments { get; set; }
        public List<int> FollowProducts { get; set; }
        public List<int> SentMessages { get; set; }
        public List<int> ReceivedMessages { get; set; }
        public List<int> Products { get; set; }
        public List<int> Reports { get; set; }
        public List<int> Reports1 { get; set; }
        public List<int> VoteLogs { get; set; }

        public UserApi()
        {
            Comments = new List<int>();
            FollowProducts = new List<int>();
            SentMessages = new List<int>();
            ReceivedMessages = new List<int>();
            Products = new List<int>();
            Reports = new List<int>();
            Reports1 = new List<int>();
            VoteLogs = new List<int>();
        }

        public UserApi(User u)
        {
            Comments = new List<int>();
            FollowProducts = new List<int>();
            SentMessages = new List<int>();
            ReceivedMessages = new List<int>();
            Products = new List<int>();
            Reports = new List<int>();
            Reports1 = new List<int>();
            VoteLogs = new List<int>();

            if (u != null)
            {
                Id = u.Id;
                UserName = u.UserName;
                Password = u.Password;
                FirstName = u.FirstName;
                LastName = u.LastName;
                Admin = u.Admin;
                Ban = u.Ban;
                Rating = u.Rating;
                Phone = u.Phone;
                ReportId = u.ReportId;
                ActiveCode = u.ActiveCode;
                ResetPassword = u.ResetPassword;
                DateRequest = u.DateRequest;
                BanDate = u.BanDate;
                BanTime = u.BanTime;
                UserImage = u.UserImage;
                Email = u.Email;

                if (u.Comments != null)
                {
                    foreach (var c in u.Comments)
                    {
                        if (c != null)
                        {
                            Comments.Add(c.Id);
                        }
                    }
                }

                if (u.FollowProducts != null)
                {
                    foreach (var c in u.FollowProducts)
                    {
                        if (c != null)
                        {
                            FollowProducts.Add(c.Id);
                        }
                    }
                }

                if (u.SentMessages != null)
                {
                    foreach (var c in u.SentMessages)
                    {
                        if (c != null)
                        {
                            SentMessages.Add(c.Id);
                        }
                    }
                }

                if (u.ReceivedMessages != null)
                {
                    foreach (var c in u.ReceivedMessages)
                    {
                        if (c != null)
                        {
                            ReceivedMessages.Add(c.Id);
                        }
                    }
                }

                if (u.Products != null)
                {
                    foreach (var c in u.Products)
                    {
                        if (c != null)
                        {
                            Products.Add(c.Id);
                        }
                    }
                }

                if (u.Reports != null)
                {
                    foreach (var c in u.Reports)
                    {
                        if (c != null)
                        {
                            Reports.Add(c.Id);
                        }
                    }
                }

                if (u.Reports1 != null)
                {
                    foreach (var c in u.Reports1)
                    {
                        if (c != null)
                        {
                            Reports1.Add(c.Id);
                        }
                    }
                }

                if (u.VoteLogs != null)
                {
                    foreach (var c in u.VoteLogs)
                    {
                        if (c != null)
                        {
                            VoteLogs.Add(c.Id);
                        }
                    }
                }
            }
        }
    }
}
