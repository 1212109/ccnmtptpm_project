﻿namespace DataTransferLayer.Accounts.Api
{
    public class VoteLogApi
    {
        public int Id { get; set; }
        public int SectionId { get; set; }
        public int? VoteForId { get; set; }
        public string UserName { get; set; }
        public int Vote { get; set; }
        public bool Active { get; set; }

        public VoteLogApi()
        {
            
        }

        public VoteLogApi(VoteLog v)
        {
            if (v != null)
            {
                Id = v.Id;
                SectionId = v.SectionId;
                VoteForId = v.VoteForId;
                UserName = v.UserName;
                Vote = v.Vote;
                Active = v.Active;
            }
        }
    }
}
