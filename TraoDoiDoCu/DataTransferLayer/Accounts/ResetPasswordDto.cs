﻿namespace DataTransferLayer.Accounts
{
    public class ResetPasswordDto
    {
        public string Password { get; set; }
        public string Email { get; set; }
        public string ResetCode { get; set; }
    }
}
