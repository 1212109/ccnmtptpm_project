﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataTransferLayer.Products.Api;

namespace DataTransferLayer.Products
{
    public class Category
    {
        public Category()
        {
            Products = new HashSet<Product>();
        }

        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public byte[] PictureCategory { get; set; }

        public HashSet<Product> Products { get; set; }

        public Category(CategoryApi c)
        {
            if (c != null)
            {
                Id = c.Id;
                Name = c.Name;
                Description = c.Description;
                PictureCategory = c.PictureCategory;
            }            

            Products = new HashSet<Product>();
        }
    }
}
