﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataTransferLayer.Products.Api;

namespace DataTransferLayer.Products
{
    public class City
    {
        public City()
        {
            Products = new HashSet<Product>();
        }

        public City(CityApi c)
        {
            if (c != null)
            {
                Id = c.Id;
                Name = c.Name;
            }           

            Products = new HashSet<Product>();
        }

        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public HashSet<Product> Products { get; set; }
    }
}
