﻿namespace DataTransferLayer.Products.Api
{
    public class CityApi
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public CityApi()
        {
            
        }

        public CityApi(City c)
        {
            if (c != null)
            {
                Id = c.Id;
                Name = c.Name;
            }           
        }
    }
}
