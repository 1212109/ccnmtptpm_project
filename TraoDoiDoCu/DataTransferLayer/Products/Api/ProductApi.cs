﻿using System;
using System.Collections.Generic;

namespace DataTransferLayer.Products.Api
{
    public class ProductApi
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }
        public decimal Price { get; set; }
        public string Picture { get; set; }
        public DateTime PostingDate { get; set; }
        public bool? Available { get; set; }
        public int UserId { get; set; }
        public string ProductKey { get; set; }
        public int Location { get; set; }     
        public List<int> Comments { get; set; }
        public List<int> FollowProducts { get; set; }
        public List<int> ProductImages { get; set; }
        public List<int> Reports { get; set; }

        public ProductApi()
        {
            Comments = new List<int>();
            FollowProducts = new List<int>();
            ProductImages = new List<int>();
            Reports = new List<int>();
        }

        public ProductApi(Product p)
        {
            Comments = new List<int>();
            FollowProducts = new List<int>();
            ProductImages = new List<int>();
            Reports = new List<int>();

            if (p != null)
            {
                Id = p.Id;
                Name = p.Name;
                Description = p.Description;
                CategoryId = p.CategoryId;
                Price = p.Price;
                PostingDate = p.PostingDate;
                Available = p.Available;
                UserId = p.UserId;
                ProductKey = p.ProductKey;
                Location = p.Location;
                Picture = p.Picture;

                if (p.Comments != null)
                {
                    foreach (var c in p.Comments)
                    {
                        if(c != null)
                            Comments.Add(c.Id);
                    }
                }

                if (p.FollowProducts != null)
                {
                    foreach (var c in p.FollowProducts)
                    {
                        if (c != null)
                            FollowProducts.Add(c.Id);
                    }
                }

                if (p.ProductImages != null)
                {
                    foreach (var c in p.ProductImages)
                    {
                        if (c != null)
                            ProductImages.Add(c.Id);
                    }
                }

                if (p.Reports != null)
                {
                    foreach (var c in p.Reports)
                    {
                        if (c != null)
                            Reports.Add(c.Id);
                    }
                }
            }
        }
    }
}
