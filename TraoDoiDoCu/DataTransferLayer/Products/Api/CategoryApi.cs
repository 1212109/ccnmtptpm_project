﻿namespace DataTransferLayer.Products.Api
{
    public class CategoryApi
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public byte[] PictureCategory { get; set; }

        public CategoryApi()
        {
            
        }

        public CategoryApi(Category c)
        {
            if (c != null)
            {
                Id = c.Id;
                Name = c.Name;
                Description = c.Description;
                PictureCategory = c.PictureCategory;
            }          
        }
    }
}
