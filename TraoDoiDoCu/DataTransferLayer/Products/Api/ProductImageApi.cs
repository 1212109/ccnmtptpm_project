﻿namespace DataTransferLayer.Products.Api
{
    public class ProductImageApi
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string ImageLink { get; set; }

        public ProductImageApi()
        {
            
        }

        public ProductImageApi(ProductImage p)
        {
            if (p != null)
            {
                Id = p.Id;
                ProductId = p.ProductId;
                ImageLink = p.ImageLink;
            }         
        }
    }
}
