﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataTransferLayer.Products.Api;

namespace DataTransferLayer.Products
{
    public class ProductImage
    {
        [Key]
        public int Id { get; set; }

        public int ProductId { get; set; }
        //[ForeignKey("ProductId")]
        public Product Product { get; set; }

        public string ImageLink { get; set; }

        public ProductImage()
        {
            
        }

        public ProductImage(ProductImageApi p)
        {
            if (p != null)
            {
                Id = p.Id;
                ProductId = p.ProductId;
                ImageLink = p.ImageLink;
            }           
        }
    }
}
