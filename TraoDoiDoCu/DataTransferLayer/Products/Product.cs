﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataTransferLayer.Accounts;
using DataTransferLayer.Products.Api;

namespace DataTransferLayer.Products
{
    public class Product
    {
        public Product()
        {
            Comments = new HashSet<Comment>();
            FollowProducts = new HashSet<FollowProduct>();
            ProductImages = new HashSet<ProductImage>();
            Reports = new HashSet<Report>();
        }

        public Product(ProductApi p)
        {
            if (p != null)
            {
                Id = p.Id;
                Name = p.Name;
                Description = p.Description;
                CategoryId = p.CategoryId;
                Price = p.Price;
                Picture = p.Picture;
                PostingDate = p.PostingDate;
                Available = p.Available;
                UserId = p.UserId;
                ProductKey = p.ProductKey;
                Location = p.Location;
            }         

            Comments = new HashSet<Comment>();
            FollowProducts = new HashSet<FollowProduct>();
            ProductImages = new HashSet<ProductImage>();
            Reports = new HashSet<Report>();
        }

        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int CategoryId { get; set; }
        //[ForeignKey("CategoryId")]
        public Category Category { get; set; }

        public decimal Price { get; set; }

        public string Picture { get; set; }

        public DateTime PostingDate { get; set; }

        public bool? Available { get; set; }

        public int UserId { get; set; }
        //[ForeignKey("UserId")]
        //[InverseProperty("Products")]
        //public User User { get; set; }
         
        public string ProductKey { get; set; }

        public int Location { get; set; }
        //[ForeignKey("Location")]
        //[InverseProperty("Products")]
        public City City { get; set; }

        public HashSet<Comment> Comments { get; set; }

        public HashSet<FollowProduct> FollowProducts { get; set; }
       
        public HashSet<ProductImage> ProductImages { get; set; }

        public HashSet<Report> Reports { get; set; }
    }
}
